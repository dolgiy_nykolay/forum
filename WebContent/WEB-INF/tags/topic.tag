<%@ tag language="java" pageEncoding="UTF-8"%>

<%@ attribute name="topic" required="true"
	type="com.dolgiy.beans.topic.BasicTopic"%>

<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<div class="topic">
	<div class="name">
		<a href='/forum/viewtopic/<c:out value="${topic.id}"/>'>${topic.name}</a>
	</div>
	<c:if
		test="${topic.isModificationAllowed(requestScope.user.getId()) || topic.getCreator().getId() == requestScope.user.getId()}">
		<form class="remove-form" action="/forum/topic/remove"
			id="f${topic.id}" method="post">
			<input type="hidden" name="id" value="${topic.id}">
		</form>
		<input type="submit" value="" class="remove-button"
			form="f${topic.id}">
	</c:if>
	<div class="under-name">
		<a class="edit-button" href="/forum/viewprofile/${topic.getCreator().getId()}">${topic.getCreator().getName()}
			${topic.getCreator().getSurname()}</a> <span class="creation-time">${topic.getCreationTime() }</span>
	</div>
	<c:if
		test="${topic.isModificationAllowed(requestScope.user.getId()) || topic.getCreator().getId() == requestScope.user.getId()}">
		<div class="topic-modification-pannel">
			<form action="/forum/topic/update" method="post">
				<input type="hidden" name="id" value="${topic.id}">
				<input type="hidden" name="name" value="${topic.getName()}">
				<input type="hidden" name="isPrivate" value="${topic.isPrivate()}">
				<input type="hidden" name="isPinned" value="${topic.isPinned()}">
				<input type="hidden" name="isOpened" value="${topic.isOpened()}">
				<input type="hidden" name="sectionId"
					value="${topic.getParentSection().getId()}">
				<input type="hidden" name="new-name" value="${topic.getName()}">

				<input type="text"
					class="text-input input-focuse hidden name-edit-input"
					name="edit_name" value="${topic.getName()}">
				<button type="submit" name="confirm"
					class="hidden name-editing confirm-background"
					onclick="prepareUpdate(this.form,'name')"></button>
				<button class="hidden name-editing cancel-background" name="cancel"
					type="button" onclick="cancelEditingName(this.form)"></button>
				<button class="edit-button" name="edit" type="button"
					onclick="editName(this.form)">Edit name</button>
				<c:if
					test="${!topic.isModificationAllowed(requestScope.user.getId()) && topic.getCreator().getId() == requestScope.user.getId()}">
					<!-- Close -->
					<!-- Edit name -->
					<!-- Private / Non private -->
					<c:if test="${topic.isOpened() == true}">
						<input type="submit" class="edit-button" value="Close"
							onclick="prepareUpdate(this.form,'open')">
					</c:if>
					<c:if test="${topic.isPrivate() == true}">
						<input type="submit" class="edit-button" value="Make non private"
							onclick="prepareUpdate(this.form,'private')">
					</c:if>
					<c:if test="${topic.isPrivate() == false}">
						<input type="submit" class="edit-button" value="Make private"
							onclick="prepareUpdate(this.form,'private')">
					</c:if>
				</c:if>
				<c:if
					test="${topic.isModificationAllowed(requestScope.user.getId())}">
					<button class="edit-button" name="change_section" type="button"
          onclick="prepareSectionList(this.form)">Change section</button>
          <select name="select_section" class="hidden select-basic section-select">
          </select>
          <button type="submit" name="confirm_section"
          class="hidden name-editing confirm-background"
          onclick="prepareUpdate(this.form,'section')"></button>
        <button class="hidden name-editing cancel-background" name="cancel_section"
          type="button" onclick="cancelEditingSection(this.form)"></button>
          
					<c:if test="${topic.isPinned() == true}">
						<input type="submit" class="edit-button" value="Unpin"
							onclick="prepareUpdate(this.form,'pinned')">
					</c:if>
					<c:if test="${topic.isPinned() == false}">
						<input type="submit" class="edit-button" value="Pin"
							onclick="prepareUpdate(this.form,'pinned')">
					</c:if>
					<c:if test="${topic.isPrivate() == true}">
						<input type="submit" class="edit-button" value="Make non private"
							onclick="prepareUpdate(this.form,'private')">
					</c:if>
					<c:if test="${topic.isPrivate() == false}">
						<input type="submit" class="edit-button" value="Make private"
							onclick="prepareUpdate(this.form,'private')">
					</c:if>
					<c:if test="${topic.isOpened() == true}">
						<input type="submit" class="edit-button" value="Close"
							onclick="prepareUpdate(this.form,'open')">
					</c:if>
					<c:if test="${topic.isOpened() == false}">
						<input type="submit" class="edit-button" value="Open"
							onclick="prepareUpdate(this.form,'open')">
					</c:if>
				</c:if>
			</form>
		</div>
	</c:if>
</div>

