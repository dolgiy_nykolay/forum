<%@ tag language="java" pageEncoding="UTF-8"%>

<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<%@ attribute name="messages" required="true" type="java.util.Queue"%>
<%@ attribute name="locale"%>

<fmt:setLocale value="${locale}" />
<fmt:setBundle basename="com.dolgiy.stringResources.Strings" />
<link rel="stylesheet" href="/forum/views/css/message-styles.css">
<script type="text/javascript" src="/forum/views/js/message-control.js"></script>

<div class="messages-container">
	<c:forEach items="${messages}" var="msg">
		<div
			class="message<c:if test="${msg.getMessageType() == 'ERROR'}"> error</c:if><c:if test="${msg.getMessageType() == 'WARNING'}"> warning</c:if><c:if test="${msg.getMessageType() == 'INFO'}"> info</c:if>">
			<div>
				<fmt:message key="${msg.messageKey}"></fmt:message>
			</div>
		</div>
	</c:forEach>
</div>
