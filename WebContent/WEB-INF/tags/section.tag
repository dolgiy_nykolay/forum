<%@ tag language="java" pageEncoding="UTF-8"%>
<%@ attribute name="section" required="true"
	type="com.dolgiy.beans.section.BasicSection"%>
<%@ attribute name="isShowControl" required="false"%>

<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<div
	class="section <c:if test="${isPinned == true}">
        pinned
    </c:if> 
    <c:if test="${section.isPrivate() == true}">
        private
    </c:if>">
	<div>
		<div class="name">
			<a href='/forum/viewsection/<c:out value="${section.id}"/>'> <c:out
					value="${section.name}"></c:out>
			</a>
		</div>
		<div class="creation-time">
			<c:out value="${section.creationTime}"></c:out>
		</div>
	</div>
	<c:if test="${isShowControl == true}">
		<form class="remove-form" action="/forum/section/remove"
			id="f${section.id}" method="post">
			<input type="hidden" name="id" value="${section.id}">
		</form>
		<input type="submit" value="" class="remove-button"
			form="f${section.id}">

		<form class="update-form" action="/forum/section/update" method="post">
			<input type="hidden" name="id" value="${section.id}">
			<input type="hidden" name="name" value="${section.name}">
			<input type="text" class="text-input input-focuse hidden name-edit-input"
				name="edit_name" value="${section.name}">
			<button type="submit" name="confirm"
				class="hidden name-editing confirm-background"
				onclick="prepareUpdate(this.form,'name')"></button>
			<button class="hidden name-editing cancel-background" name="cancel"
				type="button" onclick="cancelEditingName(this.form)"></button>
			<button class="edit-button" name="edit" type="button"
				onclick="editName(this.form)">Edit name</button>
			<input type="hidden" name="isPrivate" value="${section.isPrivate()}">
			<input type="hidden" name="isPinned" value="${section.isPinned()}">
			<input type="hidden" name="isOpened" value="${section.isOpened()}">
			<c:if test="${section.isPinned() == true}">
				<input type="submit" class="edit-button" value="Unpin"
					onclick="prepareUpdate(this.form,'pinned')">
			</c:if>
			<c:if test="${section.isPinned() == false}">
				<input type="submit" class="edit-button" value="Pin"
					onclick="prepareUpdate(this.form,'pinned')">
			</c:if>
			<c:if test="${section.isPrivate() == true}">
				<input type="submit" class="edit-button" value="Make non private"
					onclick="prepareUpdate(this.form,'private')">
			</c:if>
			<c:if test="${section.isPrivate() == false}">
				<input type="submit" class="edit-button" value="Make private"
					onclick="prepareUpdate(this.form,'private')">
			</c:if>
			<c:if test="${section.isOpened() == true}">
				<input type="submit" class="edit-button" value="Close"
					onclick="prepareUpdate(this.form,'open')">
			</c:if>
			<c:if test="${section.isOpened() == false}">
				<input type="submit" class="edit-button" value="Open"
					onclick="prepareUpdate(this.form,'open')">
			</c:if>
		</form>

		<c:if test="${isPinned == true}">

		</c:if>
	</c:if>
</div>
