<%@ tag language="java" pageEncoding="UTF-8"%>

<%@ attribute name="post" required="true"
	type="com.dolgiy.beans.post.BasicPost"%>
<%@ attribute name="isShowControl" required="false"%>

<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<div class="post">
	<div class="post-user-part">
		<img class="post-user-img" alt="img" src="/forum/viewimage/${post.getCreator().getCurrentProfilePhotoId()}">
		<div>
			<a class="edit-button" href="/forum/viewprofile/${post.getCreator().getId()}">${post.getCreator().getName()}
				${post.getCreator().getSurname()}</a>
		</div>
		<div>${post.getCreator().getUserType()}</div>
	</div>
	<div class="post-content-part">
		<p>${post.getContent()}</p>
	</div>
	<div class="post-statistic">
		<div>${post.getCreationTime()}</div>
		<div>Likes</div>
		<c:if
			test="${post.getTopic().isModificationAllowed(requestScope.user.getId())}">
			<form action="/forum/post/remove" method="post">
				<input type="hidden" name="postId" value="${post.getId()}">
				<input class="edit-button" type="submit" value="Delete">
			</form>
		</c:if>
	</div>
</div>