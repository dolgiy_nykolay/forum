<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="mt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<link rel="stylesheet" href="/forum/views/css/main.css">
<link rel="stylesheet" href="/forum/views/css/post-styles.css">
</head>
<body>

	<jsp:include page="parts/Header.jsp"></jsp:include>

	<mt:messages messages="${sessionScope.messages}"
		locale="${requestScope.language}"></mt:messages>

	<div class="under-header-links-part">
		<a href="/forum">Main page</a> <a>/</a> <a
			href="/forum/viewsection/${requestScope.parentTopic.getParentSection().getId()}">${requestScope.parentTopic.getParentSection().getName()}</a>
		<a>/</a> <a>${requestScope.parentTopic.getName()}</a>
	</div>

	<main> <c:if test="${requestScope.posts.size() == 0}">
		<div>There are not posts in this topic.</div>
	</c:if> <c:forEach items="${requestScope.posts}" var="post">
		<mt:post post="${post}"></mt:post>
	</c:forEach>

	<div class="page_choice">
		<a>[</a>
		<c:if test="${requestScope.page > 1}">
			<div>
				<a
					href="/forum/viewtopic/${requestScope.parentTopic.getId()}?page=1">1</a>
			</div>
		</c:if>
		<c:if test="${requestScope.page > 2}">
			<div>
				<a
					href="/forum/viewtopic/${requestScope.parentTopic.getId()}?page=2">2</a>
			</div>
		</c:if>
		<c:if test="${requestScope.page > 4}">
			<div>
				<a>...</a>
			</div>
		</c:if>
		<c:if test="${requestScope.page > 3}">
			<div>
				<a
					href="/forum/viewtopic/${requestScope.parentTopic.getId()}?page=${requestScope.page - 1}">${requestScope.page - 1}</a>
			</div>
		</c:if>
		<div>
			<a class="current_page">${requestScope.page}</a>
		</div>
		<c:if test="${requestScope.page < requestScope.pages - 2}">
			<div>
				<a
					href="/forum/viewtopic/${requestScope.parentTopic.getId()}?page=${requestScope.page + 1}">${requestScope.page + 1}</a>
			</div>
		</c:if>
		<c:if test="${requestScope.page < requestScope.pages - 3}">
      <div>
        <a>...</a>
      </div>
    </c:if>
		<c:if test="${requestScope.page < requestScope.pages - 1}">
			<div>
				<a
					href="/forum/viewtopic/${requestScope.parentTopic.getId()}?page=${requestScope.pages - 1}">${requestScope.pages - 1}</a>
			</div>
		</c:if>
		<c:if test="${requestScope.page < requestScope.pages}">
			<div>
				<a
					href="/forum/viewtopic/${requestScope.parentTopic.getId()}?page=${requestScope.pages}">${requestScope.pages}</a>
			</div>
		</c:if>
		<a>]</a>
	</div>


	<c:if
		test="${requestScope.user.getUserType() != 'GUEST' && (parentTopic.isOpened() || parentTopic.isModificationAllowed(requestScope.user.getId()))}">
		<div class="post-add">
			<div>Add new post:</div>
			<form class="post" action="/forum/post/add" method="post">
				<div class="post-user-part">
					<img class="post-user-img" alt="img"
						src="/forum/viewimage/${requestScope.user.getCurrentProfilePhotoId()}">
					<div>${requestScope.user.getName()}
						${requestScope.user.getSurname()}</div>
					<div>${requestScope.user.getUserType()}</div>
				</div>
				<div class="post-content-part">
					<input type="hidden" name="topicId"
						value="${requestScope.parentTopic.getId()}">
					<input type="hidden" name="relatedPost" value="-1">
					<textarea class="post-textarea" name="content" cols="" rows=""></textarea>
				</div>
				<div class="post-statistic">
					<input class="button-focuse submit-button" type="submit"
						value="Add new">
				</div>
			</form>
		</div>
	</c:if> </main>

	<div class="glass"></div>
</body>
<c:remove var="posts" scope="request" />
<c:remove var="parentTopic" scope="request" />
<c:remove var="pages" scope="request" />
<c:remove var="page" scope="request" />

<c:remove var="notPinnedTopics" scope="request" />
<c:remove var="sectionId" scope="request" />
</html>