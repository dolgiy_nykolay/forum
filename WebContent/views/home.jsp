<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="mt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Forum</title>
<link rel="stylesheet" href="/forum/views/css/main.css">
</head>
<body>

	<jsp:include page="parts/Header.jsp"></jsp:include>

	<mt:messages messages="${sessionScope.messages}"
		locale="${requestScope.language}"></mt:messages>

	<main>
	<link rel="stylesheet" href="/forum/views/css/section-styles.css">
	<div>
		<script type="text/javascript" src="/forum/views/js/updating-control.js"></script>
		<div class="pinned-sections-block">
			<div>Pinned sections:</div>
			<c:forEach items="${requestScope.pinnedSections}" var="section">
				<mt:section section="${section}"
					isShowControl="${section.isModificationAllowed(requestScope.user.id)}"></mt:section>
			</c:forEach>
		</div>
		<c:forEach items="${requestScope.notPinnedSections}" var="section">
			<mt:section section="${section}"
				isShowControl="${section.isModificationAllowed(requestScope.user.id)}"></mt:section>
		</c:forEach>


		<c:if test="${requestScope.user.userType == 'ADMIN' }">
			<form class="add-form" action="/forum/section/add"
				method="post">
				<div>Create new section</div>
				<input class="text-input input-focuse" type="text" name="name"
					placeholder="Name">
				<input type="checkbox" class="check-box-basic" id="is_private_check_box" name="isPrivate">
				<label for="is_private_check_box">Private section</label>
				<input type="checkbox" class="check-box-basic" id="is_pinned_caheck_box" name="isPinned">
				<label for="is_pinned_caheck_box">Pin section</label>
				<input class="button-focuse submit-button" type="Submit"
					value="Create">
			</form>
		</c:if>

	</div>
	</main>
	<div class="glass"></div>
</body>
<c:remove var="pinnedSections" scope="request" />
<c:remove var="notPinnedSections" scope="request" />
</html>