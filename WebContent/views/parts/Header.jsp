<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="mt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<link href="https://fonts.googleapis.com/css?family=Cutive+Mono"
	rel="stylesheet">
<link rel="stylesheet" href="/forum/views/css/header.css">
</head>
<body>
	<fmt:setLocale value="${requestScope.language}" />
	<fmt:setBundle basename="com.dolgiy.stringResources.Strings" />
	<header>
	<div class="header-left">
		<div class="forum-logo position-center">Forum</div>
	</div>
	<div class="header-right">
		<div class="right-upper-part">
			<div class="lang-select-block">
				<form action="/forum/lang" method="post">
					<select name="lang" onchange="this.form.submit()"
						class="select-basic lang-select">
						<option value="ru-RU"
							<c:if test="${requestScope.language == 'ru-RU'}"> <c:out value="selected='selected'"></c:out> </c:if>>Русский</option>
						<option value="en"
							<c:if test="${requestScope.language == 'en'}"><c:out value="selected='selected'"></c:out></c:if>>English</option>
					</select>
				</form>
			</div>
		</div>
		<div class="user-auth-block">
			<c:if test="${requestScope.user.userType == 'GUEST'}">
				<form action="/forum/user/signin" method="post">
					<input class="text-input input-focuse" type="text" name="login"
						placeholder="<fmt:message key="user.login-text"/>"
						value="${sessionScope.login}">
					<input class="text-input input-focuse" type="password"
						name="password"
						placeholder="<fmt:message key="user.password-text"/>">
					<input type="submit" class="submit-button button-focuse"
						value="<fmt:message key="user.sign-in-text"/>">
					<a href="/forum/user/signup"
						class="link-button button-focuse sign-up-link"><fmt:message
							key="user.sign-up-text" /></a>
				</form>
			</c:if>
			<c:if test="${requestScope.user.userType != 'GUEST'}">
				<div class="user-info-block">
					<div class="name-surname">
						<a href="/forum/viewprofile/${requestScope.user.getId()}" class="edit-button user-info-text">
							<c:out value="${requestScope.user.name}"></c:out>
							<c:out value="${requestScope.user.surname}"></c:out>
						</a>
					</div>
					<img alt="User photo" src="/forum/viewimage/${requestScope.user.getCurrentProfilePhotoId()}" class="user-photo">
					<div class="right-buttons-container">
					<form id="fsign-out-user" action="/forum/user/signout" method="post"></form>
						<button form="fsign-out-user"
							class="submit-button button-focuse"><fmt:message
								key="user.sign-out-text" /></button>
					</div>
				</div>
			</c:if>
			<c:remove var="login" scope="session" />
		</div>
	</div>
	<script type="text/javascript" src="/forum/views/js/jquery-3.3.1.min.js"></script>
	</header>
</body>
</html>