/**
 * 
 */

$(document).ready(() => {
	setTimeout(hideMessage, 3000);
});

function hideMessage() {
	$(".message").first().hide(1000,"swing",() => {
		$(".message").first().remove();
		setTimeout(hideMessage, 1500);
	});
}