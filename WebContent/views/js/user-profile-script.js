/**
 * 
 */

function prepareSectionList(form){

		$.ajax({
			url: '/forum/section/astext',
			dataType: 'json'
		}).done((data)=>{
			for (let i in data) {
				$(form.sectionId).append("<option value='"+ data[i].id +"' >"+ data[i].name +"</option>");
			}
		}).fail(()=>{
			alert("Error!");
		});
	
	
	$(form.confirm).removeClass("hidden");
	$(form.cancel).removeClass("hidden");
	$(form.sectionId).removeClass("hidden");
	
	$(form.prepare_adding).addClass("hidden");
}

function cancelEditingSection(form){
	$(form.sectionId).empty();
	$(form.sectionId).addClass("hidden");
	$(form.cancel).addClass("hidden");
	$(form.confirm).addClass("hidden");
	
	$(form.prepare_adding).removeClass("hidden");
}