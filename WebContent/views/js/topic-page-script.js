/**
 * 
 */

function prepareSectionList(form){

		$.ajax({
			url: '/forum/section/astext',
			dataType: 'json'
		}).done((data)=>{
			let disabled;
			let eClass;
			for (let i in data) {
				disabled = data[i].isOpened ? "" : "disabled";
				eClass = data[i].isOpened ? "select-option-enabled" : "select-option-disabled";
				$(form.select_section).append("<option class='"+ eClass +"' value='"+ data[i].id +"' " + disabled + ">"+ data[i].name +"</option>");
			}
		}).fail(()=>{
			alert("Error!");
		});
	
	
	$(form.confirm_section).removeClass("hidden");
	$(form.cancel_section).removeClass("hidden");
	$(form.select_section).removeClass("hidden");
	
	$(form.change_section).addClass("hidden");
}

function cancelEditingSection(form){
	$(form.select_section).empty();
	$(form.confirm_section).addClass("hidden");
	$(form.cancel_section).addClass("hidden");
	$(form.select_section).addClass("hidden");
	
	$(form.change_section).removeClass("hidden");
}