/**
 * 
 */

function editName(form) {
	$(form.edit_name).removeClass('hidden');
	$(form.cancel).removeClass('hidden');
	$(form.confirm).removeClass('hidden');
	$(form.edit).addClass('hidden');
}

function cancelEditingName(form){
	$(form.edit_name).addClass('hidden');
	$(form.cancel).addClass('hidden');
	$(form.edit).removeClass('hidden');
	$(form.confirm).addClass('hidden');
	form.edit_name.value = form.name.value;
}

function prepareUpdate(form, action) {
	switch (action) {
	case "pinned":
		if (form.isPinned.value == 'true') {
			form.isPinned.value = false;
		} else {
			form.isPinned.value = true;
		}
		break;
	case "private":
		if (form.isPrivate.value == 'true') {
			form.isPrivate.value = false;
		} else {
			form.isPrivate.value = true;
		}
		break;
	case "open":
		if (form.isOpened.value == 'true') {
			form.isOpened.value = false;
		} else {
			form.isOpened.value = true;
		}
		break;
	case "name":
		form.name.value = form.edit_name.value;
		break;
	case "section":
		form.sectionId.value = form.select_section.value;
		break;
	default:

		break;
	}

}