<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="mt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<link rel="stylesheet" href="/forum/views/css/main.css">
<link rel="stylesheet" href="/forum/views/css/user-page-styles.css">
<script type="text/javascript" src="/forum/views/js/user-profile-script.js"></script>
<body>

	<jsp:include page="parts/Header.jsp"></jsp:include>

	<mt:messages messages="${sessionScope.messages}"
		locale="${requestScope.language}"></mt:messages>

	<div class="under-header-links-part">
		<a href="/forum">Main page</a>
	</div>

	<main>
	<div class="user-page-block">
		<div class="user-page-profile-photo">
			<img alt=""
				src="/forum/viewimage/${requestScope.requestedUser.getCurrentProfilePhotoId()}">
		</div>
		<div class="user-page-main-info">
			<div class="info-block">
				<span class="info-block-title">Name: </span> <span>${requestScope.requestedUser.getName()}
					${requestScope.requestedUser.getSurname()}</span>
			</div>
			<div class="info-block">
				<span class="info-block-title">Nickname: </span> <span>${requestScope.requestedUser.getNickname()}</span>
			</div>
			<div class="info-block">
				<span class="info-block-title">User type: </span>
				<c:if
					test="${requestScope.user.getUserType() == 'ADMIN' &&  requestScope.user.getId() != requestScope.requestedUser.getId()}">
					<form action="/forum/user/setrole" method="post"
						style="display: inline;">
						<input type="hidden" name="userId"
							value="${requestScope.requestedUser.getId()}">
						<select name="role" class="select-basic"
							onchange="this.form.submit()">
							<option
								<c:if test="${requestScope.requestedUser.getUserType() == 'ADMIN'}" >selected</c:if>>ADMIN</option>
							<option
								<c:if test="${requestScope.requestedUser.getUserType() == 'MODERATOR'}" >selected</c:if>>MODERATOR</option>
							<option
								<c:if test="${requestScope.requestedUser.getUserType() == 'USER'}" >selected</c:if>>USER</option>
						</select>
					</form>
				</c:if>
				<c:if
					test="${requestScope.user.getUserType() != 'ADMIN' ||  requestScope.user.getId() == requestScope.requestedUser.getId()}">
					<span>${requestScope.requestedUser.getUserType()}</span>
				</c:if>
			</div>
			<c:if
				test="${requestScope.user.getId() == requestScope.requestedUser.getId() }">
				<div class="info-block">
					<span class="info-block-title">Email: </span> <span>${requestScope.requestedUser.getEmail()}</span>
				</div>
				<div class="info-block">
					<form action="/forum/user/setphoto" method="post"
						enctype="multipart/form-data">
						<input class="text-input input-focuse" type="file" name="img"
							placeholder="Chose new photo">
						<input type="submit" value="Confirm">
					</form>
				</div>
				<div class="info-block">
					<form action="/forum/user/setpassword" method="post">
						<input class="text-input input-focuse" type="password"
							name="oldPassword" placeholder="Old password">
						<input class="text-input input-focuse" type="password"
							name="newPassword" placeholder="New password">
						<input class="text-input input-focuse" type="password"
							name="confirmPassword" placeholder="Confirm new password">
						<input type="submit" value="Chandge password">
					</form>
				</div>
			</c:if>
			<c:if
				test="${(requestScope.user.getUserType() == 'ADMIN' || requestScope.user.getUserType() == 'MODERATOR') && requestScope.user.getId() != requestScope.requestedUser.getId()}">
				<div class="info-block">
					<form action="/forum/user/block" method="post">
						<input type="hidden" name="userId"
							value="${requestScope.requestedUser.getId()}">
						<input class="text-input input-focuse" type="text" name="cause"
							placeholder="Block message">
						<input class="text-input input-focuse" type="date" onload=""
							name="date">
						<input type="submit" value="Block">
					</form>
				</div>
			</c:if>
			<c:if
				test="${requestScope.user.getUserType() == 'ADMIN' && requestScope.requestedUser.getUserType() == 'MODERATOR'}">
				<div>Moderator of sections:</div>
				<c:forEach items="${requestScope.requestedUser.getPermissions()}"
					var="permission">
					<div class="info-block">
						<span>${permission.getSection().getName()}</span>
						<form action="/forum/user/removepermission" method="post"
							style="display: inline;">
							<input type="hidden" name="userId"
								value="${permission.getOwnerOfPermission().getId()}">
							<input type="hidden" name="sectionId"
								value="${permission.getSection().getId()}">
							<input type="submit" value="remove">
						</form>
					</div>
				</c:forEach>
				<div class="info-block">
					<div>Add section to moderation</div>
					<form action="/forum/user/addpermission" method="post">
						<button type="button" name="prepare_adding"
							onclick="prepareSectionList(this.form)">Add</button>
						<input type="hidden" name="userId"
							value="${requestScope.requestedUser.getId()}">
						<select name="sectionId" class="hidden">

						</select>
						<input name="confirm" type="submit" class="hidden" value="Add new">
						<button type="button" name="cancel" class="hidden" onclick="cancelEditingSection(this.form)">Cancel</button>
					</form>
				</div>
			</c:if>
			<div>
				<div>Block list:</div>
				<!-- List items -->
			</div>
		</div>
	</div>
	</main>

	<div class="glass"></div>
</body>
<c:remove var="requestedUser" scope="request" />
</html>