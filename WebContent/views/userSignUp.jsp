<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="mt"%>

<fmt:setLocale value="${requestScope.language}" />
<fmt:setBundle basename="com.dolgiy.stringResources.Strings" />

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Sign up</title>
<link rel="stylesheet" href="/forum/views/css/main.css">
<link rel="stylesheet" href="/forum/views/css/sign-up-page-style.css">
</head>
<body>

	<jsp:include page="parts/Header.jsp"></jsp:include>

	<mt:messages messages="${sessionScope.messages}"
		locale="${requestScope.language}"></mt:messages>

	<div class="under-header-links-part">
		<a href="/forum">Main page</a>
	</div>

	<main>
	<form action="/forum/user/signup" class="sign-up-form" method="post">
		<div class="sign-up-row">
			<input type="text" class="text-input input-focuse" name="name"
				placeholder="<fmt:message key="user.name-text"/>"
				value='<c:out value="${sessionScope.name}"></c:out>'>
		
			<input type="text" class="text-input input-focuse" name="surname"
				placeholder="<fmt:message key="user.surname-text"/>"
				value='<c:out value="${sessionScope.surname}"></c:out>'>
		</div>
		<div class="sign-up-row">
			<input type="text" class="text-input input-focuse" name="nickname"
				placeholder="<fmt:message key="user.nickname-text"/>"
				value='<c:out value="${sessionScope.nickname}"></c:out>'>
		</div>
		<div class="sign-up-row">
			<input type="text" class="text-input input-focuse" name="email"
				placeholder="<fmt:message key="user.email-text"/>"
				value='<c:out value="${sessionScope.email}"></c:out>'>
		</div>
		<div class="sign-up-row">
			<input type="password" class="text-input input-focuse" name="pass"
				placeholder="<fmt:message key="user.password-text"/>">
	
			<input type="password" class="text-input input-focuse" name="pass-confirm"
				placeholder="<fmt:message key="user.confirm-password-text"/>">
		</div>
		<div class="sign-up-row">
			<input type="submit" class="submit-button button-focuse" value="<fmt:message key="user.sign-up-text"/>">
		</div>
	</form>
	</main>
	<div class="glass"></div>
</body>

<c:remove var="name" scope="session" />
<c:remove var="surname" scope="session" />
<c:remove var="nickname" scope="session" />
<c:remove var="email" scope="session" />

</html>