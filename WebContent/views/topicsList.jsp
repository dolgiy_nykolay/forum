<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="mt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Forum</title>
<link rel="stylesheet" href="/forum/views/css/main.css">
<link rel="stylesheet" href="/forum/views/css/topic-styles.css">
<script type="text/javascript" src="/forum/views/js/updating-control.js"></script>
</head>
<body>

	<jsp:include page="parts/Header.jsp"></jsp:include>

	<mt:messages messages="${sessionScope.messages}"
		locale="${requestScope.language}"></mt:messages>

	<div class="under-header-links-part">
		<a href="/forum">Main page</a>
		<a>/</a>
		<a>${section.getName()}</a>
	</div>


	<main>
	 <c:if
		test="${requestScope.pinnedTopics.size() == 0 && requestScope.notPinnedTopics.size() == 0}">
		<div class="section-name-in-topics-list">There are not topic in this section.</div>
	</c:if> <c:forEach items="${requestScope.pinnedTopics}" var="topic">
		<mt:topic topic="${topic}"></mt:topic>
	</c:forEach> <c:forEach items="${requestScope.notPinnedTopics}" var="topic">
		<mt:topic topic="${topic}"></mt:topic>
	</c:forEach> <c:if
		test="${(requestScope.section.isModificationAllowed(requestScope.user.getId()) && !requestScope.section.isOpened()) || (requestScope.section.isOpened() && requestScope.user.userType != 'GUEST')}">
		<form action="/forum/topic/add" class="add-form" method="post">
			<div>Create new section</div>
			<input type="hidden" name="sectionId" value="${requestScope.section.getId()}">
			<input type="text" class="text-input input-focuse" name="name"
				placeholder="Name">
			<input type="checkbox" class="check-box-basic" id="is_private_check_box" name="isPrivate">
			<label for="is_private_check_box">Private topic</label>
			<input type="Submit" class="button-focuse submit-button"
				value="Create">
		</form>
	</c:if> </main>

	<div class="glass"></div>
	<script type="text/javascript" src="/forum/views/js/topic-page-script.js"></script>
</body>

<c:remove var="pinnedTopics" scope="request" />
<c:remove var="notPinnedTopics" scope="request" />
<c:remove var="section" scope="request" />
</html>