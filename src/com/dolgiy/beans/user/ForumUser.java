package com.dolgiy.beans.user;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import com.dolgiy.beans.StatisticControlProvider;
import com.dolgiy.beans.section.BasicSection;
import com.dolgiy.beans.user.block.Block;
import com.dolgiy.beans.user.permission.Permission;

public class ForumUser implements User, StatisticControlProvider {

    private long id = -1;
    private UserType userType;
    private String name;
    private String surname;
    private String nickname;
    private String email;
    private long currentProfilePhotoId;
    private List<Block> activeBolcks;
    private List<Permission> permissionList;

    private int likes;
    private int dislikes;

    public ForumUser () {
        this.userType = UserType.BasicType.GUEST;
        this.activeBolcks = new ArrayList<>();
        this.permissionList = new ArrayList<>();
    }

    public ForumUser (long id, UserType type) {
        this.id = id;
        this.userType = type;
        this.activeBolcks = new ArrayList<>();
        this.permissionList = new ArrayList<>();
    }

    public String getName () {
        return name;
    }

    public void setName (String name) {
        this.name = name;
    }

    public String getSurname () {
        return surname;
    }

    public void setSurname (String surname) {
        this.surname = surname;
    }

    public String getNickname () {
        return nickname;
    }

    public void setNickname (String nickname) {
        this.nickname = nickname;
    }

    public String getEmail () {
        return email;
    }

    public void setEmail (String email) {
        this.email = email;
    }

    public long getCurrentProfilePhotoId () {
        return currentProfilePhotoId;
    }

    public void setCurrentProfilePhotoId (long currentProfilePhotoId) {
        this.currentProfilePhotoId = currentProfilePhotoId;
    }

    public int getLikes () {
        return likes;
    }

    public void setLikes (int likes) {
        this.likes = likes;
    }

    public int getDislikes () {
        return dislikes;
    }

    public void setDislikes (int dislikes) {
        this.dislikes = dislikes;
    }

    public long getId () {
        return id;
    }

    protected void setId (long id) {
        this.id = id;
    }

    public UserType getUserType () {
        return userType;
    }

    protected void setUserType (UserType type) {
        this.userType = type;
    }

    @Override
    public String toString () {
        return "User {id=" + id + ", userType=" + userType + ", name=" + name + ", surname=" + surname + ", nickname="
                + nickname + ", email=" + email + "}";
    }

    @Override
    public Collection<Block> getActiveBlocks () {
        return Collections.unmodifiableList(this.activeBolcks);
    }

    @Override
    public void addBlock (Block block) {
        this.activeBolcks.add(block);
    }

    @Override
    public Collection<Permission> getPermissions () {
        return Collections.unmodifiableCollection(this.permissionList);
    }

    @Override
    public void addPermission (Permission permission) {
        this.permissionList.add(permission);
        
    }

}
