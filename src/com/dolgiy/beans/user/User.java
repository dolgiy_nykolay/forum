package com.dolgiy.beans.user;

import java.util.Collection;

import com.dolgiy.beans.user.block.Block;
import com.dolgiy.beans.user.permission.Permission;

/**
 * 
 * 
 *
 */
public interface User{

    public String getName ();

    public void setName (String name);

    public String getSurname ();

    public void setSurname (String surname);

    public String getNickname ();

    public void setNickname (String nickname);

    public String getEmail ();

    public void setEmail (String email);
    
    public long getId ();
    
    public UserType getUserType ();
    
    public Collection<Block> getActiveBlocks();
    
    public void addBlock(Block block);
    
    public Collection<Permission> getPermissions();
    
    public void addPermission(Permission permission);
}
