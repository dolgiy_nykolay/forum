package com.dolgiy.beans.user;

public interface UserType{
    
     enum BasicType implements UserType{
         GUEST, ADMIN, MODERATOR, USER
     }
     
}
