package com.dolgiy.beans.user.block;

import java.util.Collection;
import java.util.Collections;

public class BlockedException extends Exception{

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    private Collection<Block> blocks;

    public BlockedException (Collection<Block> blocks) {
        super();
        this.blocks = blocks;
    }
    
    public Collection<Block> getBlocks(){
        return Collections.unmodifiableCollection(this.blocks);
    }
}
