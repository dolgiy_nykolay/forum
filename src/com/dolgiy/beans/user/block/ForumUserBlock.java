package com.dolgiy.beans.user.block;

import java.util.Date;

import com.dolgiy.beans.user.User;

public class ForumUserBlock implements Block{

    private String message;
    private User executor;
    private Date expireDate;
    
    public ForumUserBlock (String message, User executor, Date expireDate) {
        super();
        this.message = message;
        this.executor = executor;
        this.expireDate = expireDate;
    }

    @Override
    public String getMessage () {
        return this.message;
    }

    @Override
    public User getExecutor () {
        return this.executor;
    }

    @Override
    public Date getExpireDate () {
        return this.expireDate;
    }
    
    
}
