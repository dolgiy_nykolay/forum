package com.dolgiy.beans.user.block;

import java.util.Date;

import com.dolgiy.beans.user.User;

public interface Block {

    String getMessage();
    
    User getExecutor();
    
    Date getExpireDate();
    
}
