package com.dolgiy.beans.user.permission;

import java.util.Date;

import com.dolgiy.beans.section.BasicSection;
import com.dolgiy.beans.user.User;

public interface Permission {
    
    BasicSection getSection();
    
    User getOwnerOfPermission();
    
    Date getCreationDate();
    
    Date getDeactivationDate();
    
    User getCreator();
    
    User getDeactivator();

}
