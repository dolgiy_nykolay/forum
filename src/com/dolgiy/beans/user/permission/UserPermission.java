package com.dolgiy.beans.user.permission;

import java.util.Date;

import com.dolgiy.beans.section.BasicSection;
import com.dolgiy.beans.user.User;

public class UserPermission implements Permission{
    
    protected BasicSection section;
    protected User ownerOfPermission;
    protected User creator;
    protected User deactivator;
    protected Date creationDate;
    protected Date deactivationDate;
    
    
    public UserPermission (BasicSection section, User ownerOfPermission) {
        super();
        this.section = section;
        this.ownerOfPermission = ownerOfPermission;
    }

    @Override
    public User getCreator () {
        return creator;
    }


    public void setCreator (User creator) {
        this.creator = creator;
    }

    @Override
    public User getDeactivator () {
        return deactivator;
    }


    public void setDeactivator (User deactivator) {
        this.deactivator = deactivator;
    }

    @Override
    public Date getCreationDate () {
        return creationDate;
    }


    public void setCreationDate (Date creationDate) {
        this.creationDate = creationDate;
    }

    @Override
    public Date getDeactivationDate () {
        return deactivationDate;
    }


    public void setDeactivationDate (Date deactivationDate) {
        this.deactivationDate = deactivationDate;
    }

    @Override
    public BasicSection getSection () {
        return section;
    }

    @Override
    public User getOwnerOfPermission () {
        return ownerOfPermission;
    }
    
    
    
}
