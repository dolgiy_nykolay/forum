package com.dolgiy.beans.image;

import java.io.InputStream;

public interface Image {
    
    InputStream getImage();
    
    String getMimeType();
    
    void setMimeType(String mimeType);
    
}
