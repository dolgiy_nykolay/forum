package com.dolgiy.beans.image;

import java.io.InputStream;

public class ForumImage implements Image{

    private InputStream img;
    private String mimeType;
    
    public ForumImage (InputStream img, String mimeType) {
        this(img);
        this.mimeType = mimeType;
    }
    
    public ForumImage (InputStream img) {
        this.img = img;
    }
    
    @Override
    public InputStream getImage () {
        return this.img;
    }

    @Override
    public String getMimeType () {
        return this.mimeType;
    }

    @Override
    public void setMimeType (String mimeType) {
        this.mimeType = mimeType;
    }
}
