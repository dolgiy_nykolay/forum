package com.dolgiy.beans;

/**
 * 
 * Object that can store statistic about likes and dislikes about itself.
 *
 */
public interface StatisticControlProvider {

    /**
     * Returns likes count.
     * 
     * @return likes count.
     */
    public int getLikes ();

    /**
     * Sets the likes count.
     * 
     * @param likes
     *            likes count.
     */
    public void setLikes (int likes);

    /**
     * Returns dislikes count.
     * 
     * @return dislikes count.
     */
    public int getDislikes ();

    /**
     * Sets the dislikes count.
     * 
     * @param dislikes
     *            dislikes count.
     */
    public void setDislikes (int dislikes);
}
