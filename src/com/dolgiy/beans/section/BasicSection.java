package com.dolgiy.beans.section;

import java.util.Date;
import java.util.List;

/**
 * 
 * 
 *
 */
public interface BasicSection {

    /**
     * 
     * @return
     */
    public String getName ();

    /**
     * 
     * @param name
     */
    public void setName (String name);

    /**
     * 
     * @return
     */
    public long getCreatorId ();

    /**
     * 
     * @param creator_id
     */
    public void setCreatorId (long creator_id);

    /**
     * 
     * @return
     */
    public boolean isPrivate ();

    /**
     * 
     * @param isPrivate
     */
    public void setPrivate (boolean isPrivate);

    /**
     * 
     * @return
     */
    public boolean isOpened ();

    /**
     * 
     * @param isOpened
     */
    public void setOpened (boolean isOpened);

    /**
     * 
     * @return
     */
    public boolean isPinned ();

    /**
     * 
     * @param isPinned
     */
    public void setPinned (boolean isPinned);

    /**
     * 
     * @return
     */
    public long getId ();

    /**
     * 
     * @return
     */
    public Date getCreationTime ();
    
    /**
     * 
     * @return
     */
    public List<Long> getModeratorsId();
    
    /**
     * 
     * @param id
     */
    public void addModeratorId(long id);
    
    /**
     * 
     * @param userId
     * @return
     */
    public boolean isModificationAllowed(long userId);
}
