package com.dolgiy.beans.section;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

/**
 * 
 * 
 *
 */
public class ForumSection implements BasicSection {

    private long id;
    private String name;
    private long creatorId;
    private boolean isPrivate;
    private boolean isOpened;
    private boolean isPinned;
    private Date creationTime;
    private List<Long> moderatorsId;
    
    /**
     * 
     * @param id
     */
    public ForumSection (long id, Date creationTime) {
        this.id = id;
        this.creationTime = creationTime;
        this.moderatorsId = new ArrayList<Long>();
    }

    /**
     * 
     */
    public ForumSection () {
        this.id = -1;
        this.moderatorsId = new ArrayList<Long>();
    }

    @Override
    public String getName () {
        return name;
    }

    @Override
    public void setName (String name) {
        this.name = name;
    }

    @Override
    public long getCreatorId () {
        return creatorId;
    }

    @Override
    public void setCreatorId (long creator_id) {
        this.creatorId = creator_id;
    }

    @Override
    public boolean isPrivate () {
        return isPrivate;
    }

    @Override
    public void setPrivate (boolean isPrivate) {
        this.isPrivate = isPrivate;
    }

    @Override
    public boolean isOpened () {
        return isOpened;
    }

    @Override
    public void setOpened (boolean isOpened) {
        this.isOpened = isOpened;
    }

    @Override
    public boolean isPinned () {
        return isPinned;
    }

    @Override
    public void setPinned (boolean isPinned) {
        this.isPinned = isPinned;
    }

    @Override
    public long getId () {
        return id;
    }

    @Override
    public Date getCreationTime () {
        return creationTime;
    }

    @Override
    public List<Long> getModeratorsId () {
        return Collections.unmodifiableList(this.moderatorsId);
    }

    @Override
    public void addModeratorId (long id) {
        this.moderatorsId.add(id);
    }

    
    @Override
    public boolean isModificationAllowed (long userId) {
        return this.moderatorsId.contains(userId);
    }

    @Override
    public String toString () {
        return "{\"id\":" + id + ", \"name\":" +"\""+ name + "\"" + ", \"isPrivate\":" + isPrivate + ", \"isOpened\":" + isOpened
                + ", \"isPinned\":" + isPinned + ", \"creationTime\":" + "\"" + creationTime + "\"" + "}";
    }

    
    
    

}
