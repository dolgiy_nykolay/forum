package com.dolgiy.beans.topic;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import com.dolgiy.beans.section.BasicSection;
import com.dolgiy.beans.user.User;


public class ForumTopic implements BasicTopic{

    private long id;
    private String name;
    private User creator;
    private BasicSection parentSection;
    private boolean isPrivate;
    private boolean isOpened;
    private boolean isPinned;
    private Date creationTime;
    private List<Long> moderatorsId;
    
    public ForumTopic (long id, Date creationTime) {
        super();
        this.id = id;
        this.creationTime = creationTime;
        this.moderatorsId = new ArrayList<>();
    }
    
    public ForumTopic () {
        super();
        this.id = -1;
        this.moderatorsId = new ArrayList<>();
    }

    @Override
    public String getName () {
        return this.name;
    }

    @Override
    public void setName (String name) {
        this.name = name;
    }

    @Override
    public User getCreator () {
        return this.creator;
    }

    @Override
    public void setCreator (User user) {
        this.creator = user;
    }

    @Override
    public boolean isPrivate () {
        return this.isPrivate;
    }

    @Override
    public void setPrivate (boolean isPrivate) {
        this.isPrivate = isPrivate;
    }

    @Override
    public boolean isOpened () {
        return this.isOpened;
    }

    @Override
    public void setOpened (boolean isOpened) {
        this.isOpened = isOpened;
    }

    @Override
    public boolean isPinned () {
        return this.isPinned;
    }

    @Override
    public void setPinned (boolean isPinned) {
        this.isPinned = isPinned;
    }

    @Override
    public long getId () {
        return this.id;
    }

    @Override
    public Date getCreationTime () {
        return this.creationTime;
    }

    @Override
    public List<Long> getModeratorsId () {
        return Collections.unmodifiableList(this.moderatorsId);
    }

    @Override
    public void addModeratorId (long id) {
        this.moderatorsId.add(id);
    }

    @Override
    public boolean isModificationAllowed (long userId) {
        return this.moderatorsId.contains(userId);
    }

    @Override
    public BasicSection getParentSection () {
        return this.parentSection;
    }

    @Override
    public void setParentSection (BasicSection section) {
        this.parentSection = section;
    }
    
    

}
