package com.dolgiy.beans.topic;

import java.util.Date;
import java.util.List;

import com.dolgiy.beans.section.BasicSection;
import com.dolgiy.beans.user.User;

public interface BasicTopic {

    /**
     * 
     * @return
     */
    public String getName ();

    /**
     * 
     * @param name
     */
    public void setName (String name);

    /**
     * 
     * @return
     */
    public User getCreator ();

    /**
     * 
     * @param id
     */
    public void setCreator (User user);

    /**
     * 
     * @return
     */
    public boolean isPrivate ();

    /**
     * 
     * @param isPrivate
     */
    public void setPrivate (boolean isPrivate);

    /**
     * 
     * @return
     */
    public boolean isOpened ();

    /**
     * 
     * @param isOpened
     */
    public void setOpened (boolean isOpened);

    /**
     * 
     * @return
     */
    public boolean isPinned ();

    /**
     * 
     * @param isPinned
     */
    public void setPinned (boolean isPinned);

    /**
     * 
     * @return
     */
    public long getId ();
    
    
    public BasicSection getParentSection();

    public void setParentSection(BasicSection section);
    
    /**
     * 
     * @return
     */
    public Date getCreationTime ();
    
    /**
     * 
     * @return
     */
    public List<Long> getModeratorsId();
    
    /**
     * 
     * @param id
     */
    public void addModeratorId(long id);
    
    /**
     * 
     * @param userId
     * @return
     */
    public boolean isModificationAllowed(long userId);
}
