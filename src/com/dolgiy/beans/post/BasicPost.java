package com.dolgiy.beans.post;

import java.util.Date;

import com.dolgiy.beans.topic.BasicTopic;
import com.dolgiy.beans.user.User;

public interface BasicPost {

    public String getContent ();

    public void setContent (String content);

    public User getCreator ();

    public void setCreator (User creator);

    public BasicTopic getTopic ();

    public void setTopic (BasicTopic topic);

    public long getRelatedPostId ();

    public void setRelatedPostId (long relatedPostId);

    public long getId ();

    public Date getCreationTime ();
}
