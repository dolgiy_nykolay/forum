package com.dolgiy.beans.post;

import java.util.Date;

import com.dolgiy.beans.topic.BasicTopic;
import com.dolgiy.beans.user.User;

public class ForumPost implements BasicPost{

    private long id;
    private String content;
    private User creator;
    private BasicTopic topic;
    private long relatedPost;
    private Date creationTime;
    
    public ForumPost() {
        super();
        this.id = -1;
        this.creationTime = null;
    }
    
    public ForumPost (long id, Date creationTime) {
        super();
        this.id = id;
        this.creationTime = creationTime;
    }

    @Override
    public String getContent () {
        return content;
    }

    @Override
    public void setContent (String content) {
        this.content = content;
    }

    @Override
    public User getCreator () {
        return creator;
    }

    @Override
    public void setCreator (User creator) {
        this.creator = creator;
    }

    @Override
    public BasicTopic getTopic () {
        return topic;
    }

    @Override
    public void setTopic (BasicTopic topic) {
        this.topic = topic;
    }

    @Override
    public long getRelatedPostId () {
        return relatedPost;
    }

    @Override
    public void setRelatedPostId (long relatedPostId) {
        this.relatedPost = relatedPostId;
    }

    @Override
    public long getId () {
        return id;
    }

    @Override
    public Date getCreationTime () {
        return creationTime;
    }
    
}
