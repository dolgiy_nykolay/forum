package com.dolgiy.filters;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;

import com.dolgiy.model.section.SectionOperations;
import com.dolgiy.model.user.ForumUserOperationsProvider;
import com.dolgiy.model.user.SecureUserOperations;
import com.dolgiy.model.user.UserOperations;

/**
 * Servlet Filter implementation class UserOperationsObjectProviderFilter
 */
@WebFilter("/UserOperationsObjectProviderFilter")
public class UserOperationsProviderFilter implements Filter {

    /**
     * @see Filter#doFilter(ServletRequest, ServletResponse, FilterChain)
     */
    public void doFilter (ServletRequest request, ServletResponse response, FilterChain chain)
            throws IOException, ServletException {

        if (request instanceof HttpServletRequest) {
            HttpServletRequest httpRequest = (HttpServletRequest) request;
            HttpSession session = httpRequest.getSession();
            if (httpRequest.getSession().getAttribute("authenticator") == null) {
                DataSource dataSource = (DataSource) session.getAttribute("dataSourse");
                SectionOperations sectionOperations = (SectionOperations) session.getAttribute("sectionOperations");
                if(sectionOperations == null) {
                    throw new NullPointerException();
                }
                Connection connection;
                try {
                    connection = dataSource.getConnection();
                } catch (SQLException e) {
                    throw new RuntimeException(e);
                }
                UserOperations operations = new SecureUserOperations(connection, sectionOperations);
                httpRequest.getSession().setAttribute("authenticator", operations);
            }
        } else {
            throw new ServletException("Given request is not HttpServletRequest.");
        }

        // pass the request along the filter chain
        chain.doFilter(request, response);
    }

}
