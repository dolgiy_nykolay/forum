package com.dolgiy.filters;

import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import com.dolgiy.beans.user.User;
import com.dolgiy.beans.user.block.BlockedException;
import com.dolgiy.model.user.SessionException;
import com.dolgiy.model.user.UserOperations;

public class AuthCheckFilter implements Filter {

    @Override
    public void doFilter (ServletRequest request, ServletResponse response, FilterChain chain)
            throws IOException, ServletException {

        if (request instanceof HttpServletRequest) {
            HttpServletRequest httpRequest = (HttpServletRequest) request;
            HttpSession session = httpRequest.getSession();

            UserOperations userOperations = (UserOperations) session.getAttribute("authenticator");            

            User currentUser = null;
            try {
                currentUser = userOperations.signIn(session.getId(), request.getRemoteAddr());
            } catch (SessionException e) {
                throw new RuntimeException(e);
            } catch (BlockedException e) {
                e.printStackTrace();
            }
            httpRequest.setAttribute("user", currentUser);
        } else {
            throw new ServletException("Given request is not HttpServletRequest.");
        }
        chain.doFilter(request, response);
    }
}
