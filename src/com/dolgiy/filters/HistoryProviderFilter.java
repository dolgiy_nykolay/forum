package com.dolgiy.filters;

import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import com.dolgiy.history.UrlHistory;

/**
 * Servlet Filter implementation class HistoryProviderFilter
 */
@WebFilter("/HistoryProviderFilter")
public class HistoryProviderFilter implements Filter {

    private int historySize = 3;
    private String defaultValue = null;

    public String getDefaultValue () {
        return defaultValue;
    }

    public void setDefaultValue (String defaultValue) {
        this.defaultValue = defaultValue;
    }

    public int getHistorySize () {
        return historySize;
    }

    public void setHistorySize (int historySize) {
        this.historySize = historySize;
    }

    @Override
    public void init (FilterConfig filterConfig) throws ServletException {
        Filter.super.init(filterConfig);

        if (filterConfig.getInitParameter("historySize") != null) {
            setHistorySize(new Integer(filterConfig.getInitParameter("historySize")));
        }

        if (filterConfig.getInitParameter("defaultValue") != null) {
            setDefaultValue(filterConfig.getInitParameter("defaultValue"));
        }
    }

    /**
     * @see Filter#doFilter(ServletRequest, ServletResponse, FilterChain)
     */
    public void doFilter (ServletRequest request, ServletResponse response, FilterChain chain)
            throws IOException, ServletException {

        if (request instanceof HttpServletRequest) {
            HttpServletRequest httpRequest = (HttpServletRequest) request;
            UrlHistory history = null;
            if (httpRequest.getSession().getAttribute("history") == null) {
                history = new UrlHistory(getHistorySize(), getDefaultValue());
                httpRequest.getSession().setAttribute("history", history);
            } else {
                history = (UrlHistory) httpRequest.getSession().getAttribute("history");
            }

            chain.doFilter(request, response);

            if (((HttpServletRequest) request).getMethod().equals("GET")) {
                if (!history.last().equals(httpRequest.getRequestURI() + "?" + (httpRequest.getQueryString() == null ? "":httpRequest.getQueryString()))) {
                    if (!httpRequest.getRequestURI().matches("(.*\\.css$)|(.*\\.js$)|(.*\\.jpg$)|(.*\\.png)|(.*astext)|(.*image.*)|(.*viewimage.*)")) {
                        history.add(httpRequest.getRequestURI() + "?" + (httpRequest.getQueryString() == null ? "":httpRequest.getQueryString()));
                    }
                }
            }
            System.out.println(history);
        } else {
            throw new ServletException("Given request is not HttpServletRequest.");
        }
    }

}
