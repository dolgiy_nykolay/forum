package com.dolgiy.filters;

import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;

/**
 * Servlet Filter implementation class LanguageSetFilter
 */
@WebFilter("/LanguageSetFilter")
public class LanguageSetFilter implements Filter {

    private String defaultLanguage = "en";

    public String getDefaultLanguage () {
        return defaultLanguage;
    }

    public void setDefaultLanguage (String defaultLanguage) {
        this.defaultLanguage = defaultLanguage;
    }

    @Override
    public void init (FilterConfig filterConfig) throws ServletException {
        Filter.super.init(filterConfig);

        if (filterConfig.getInitParameter("defaultLanguage") != null) {
            setDefaultLanguage(filterConfig.getInitParameter("defaultLanguage"));
        }
    }

    /**
     * @see Filter#doFilter(ServletRequest, ServletResponse, FilterChain)
     */
    public void doFilter (ServletRequest request, ServletResponse response, FilterChain chain)
            throws IOException, ServletException {

        if (request instanceof HttpServletRequest) {
            Cookie[] cookies = ((HttpServletRequest) request).getCookies();
            String language = defaultLanguage;
            if (cookies != null) {
                for (Cookie c : cookies) {
                    if (c.getName().equals("language")) {
                        language = c.getValue();
                        break;
                    }
                }
            }
            request.setAttribute("language", language);
            chain.doFilter(request, response);
        } else {
            throw new ServletException("Given request is not HttpServletRequest.");
        }
    }

}
