package com.dolgiy.filters;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.dolgiy.message.MessagesContainer;

/**
 * Servlet Filter implementation class MessageQueueProvider
 */
@WebFilter("/MessageQueueProvider")
public class MessageQueueProvider implements Filter {

	/**
	 * @see Filter#doFilter(ServletRequest, ServletResponse, FilterChain)
	 */
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
	
	    if (request instanceof HttpServletRequest) {
            HttpServletRequest httpRequest = (HttpServletRequest) request;
            HttpSession session = httpRequest.getSession();

            if (session.getAttribute("messages") == null) {
                session.setAttribute("messages", new MessagesContainer());
            }

        } else {
            throw new ServletException("Given request is not HttpServletRequest.");
        }

		// pass the request along the filter chain
		chain.doFilter(request, response);
	}

}
