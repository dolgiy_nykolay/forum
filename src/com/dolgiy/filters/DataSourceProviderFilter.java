package com.dolgiy.filters;

import java.io.IOException;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;


public class DataSourceProviderFilter implements Filter {

    @Override
    public void doFilter (ServletRequest request, ServletResponse response, FilterChain chain)
            throws IOException, ServletException {
        if (request instanceof HttpServletRequest) {
            HttpServletRequest httpRequest = (HttpServletRequest) request;
            HttpSession session = httpRequest.getSession();

            if (session.getAttribute("dataSourse") == null) {
                try {
                    Context context = (Context) new InitialContext().lookup("java:comp/env");
                    DataSource dataSourse = (DataSource) context.lookup("jdbc/pool/ForumDB");
                    session.setAttribute("dataSourse", dataSourse);
                } catch (NamingException e) {
                    throw new RuntimeException(e);
                }
            }

        } else {
            throw new ServletException("Given request is not HttpServletRequest.");
        }
        chain.doFilter(request, response);
    }

}
