package com.dolgiy.filters;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;

import com.dolgiy.model.post.ForumPostOperationsProvider;
import com.dolgiy.model.post.SecurePostOperations;
import com.dolgiy.model.topic.TopicOperations;
import com.dolgiy.model.user.UserOperations;

/**
 * Servlet Filter implementation class PostOperationsProviderFilter
 */
@WebFilter("/PostOperationsProviderFilter")
public class PostOperationsProviderFilter implements Filter {

    /**
     * @see Filter#doFilter(ServletRequest, ServletResponse, FilterChain)
     */
    public void doFilter (ServletRequest request, ServletResponse response, FilterChain chain)
            throws IOException, ServletException {

        if (request instanceof HttpServletRequest) {
            HttpServletRequest httpRequest = (HttpServletRequest) request;
            HttpSession session = httpRequest.getSession();
            if (session.getAttribute("postOperations") == null) {
                DataSource dataSource = (DataSource) session.getAttribute("dataSourse");
                TopicOperations topicOperations = (TopicOperations) session.getAttribute("topicOperations");
                UserOperations userOperations = (UserOperations) session.getAttribute("authenticator");
                if (topicOperations == null) {
                    throw new NullPointerException();
                }
                if(userOperations == null) {
                    throw new NullPointerException();
                }
                Connection connection;
                try {
                    connection = dataSource.getConnection();
                } catch (SQLException e) {
                    throw new RuntimeException(e);
                }
                session.setAttribute("postOperations",
                        new SecurePostOperations(connection, topicOperations, userOperations));
            }

        } else {
            throw new ServletException("Given request is not HttpServletRequest.");
        }

        // pass the request along the filter chain
        chain.doFilter(request, response);
    }

}
