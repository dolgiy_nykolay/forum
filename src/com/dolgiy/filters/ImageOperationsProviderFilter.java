package com.dolgiy.filters;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;

import com.dolgiy.model.image.ImageOperations;
import com.dolgiy.model.image.SecureImageOperations;

/**
 * Servlet Filter implementation class ImageOperationsProviderFilter
 */
@WebFilter("/ImageOperationsProviderFilter")
public class ImageOperationsProviderFilter implements Filter {

	/**
	 * @see Filter#doFilter(ServletRequest, ServletResponse, FilterChain)
	 */
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		
	    if (request instanceof HttpServletRequest) {
            HttpServletRequest httpRequest = (HttpServletRequest) request;
            HttpSession session = httpRequest.getSession();
            if (httpRequest.getSession().getAttribute("imageOperations") == null) {
                DataSource dataSource = (DataSource) session.getAttribute("dataSourse");
                Connection connection;
                try {
                    connection = dataSource.getConnection();
                } catch (SQLException e) {
                    throw new RuntimeException(e);
                }
                ImageOperations operations = new SecureImageOperations(connection);
                httpRequest.getSession().setAttribute("imageOperations", operations);
            }
        } else {
            throw new ServletException("Given request is not HttpServletRequest.");
        }
	    

		// pass the request along the filter chain
		chain.doFilter(request, response);
	}

	

}
