package com.dolgiy.model;

public class AccesException extends Exception{

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    public AccesException () {
        super();
    }

    public AccesException (String message, Throwable cause) {
        super(message, cause);
    }

    public AccesException (String message) {
        super(message);
    }

    public AccesException (Throwable cause) {
        super(cause);
    }
    
    

}
