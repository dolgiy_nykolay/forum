package com.dolgiy.model.post;

import java.util.Collection;

import com.dolgiy.beans.post.BasicPost;
import com.dolgiy.beans.user.User;
import com.dolgiy.model.AccesException;

public interface PostOperations {
    
    public Collection<BasicPost> get(User user, long topicId, long from, long to);
    
    public Collection<BasicPost> get(User user, long topicId);
    
    public void remove(User user, long postId) throws AccesException;
    
    public void add(User user, String content, long topicId) throws AccesException;
    
    public void add(User user, String content, long topicId, long relatedPost) throws AccesException;

    public int count(User user, long topicId);
}
