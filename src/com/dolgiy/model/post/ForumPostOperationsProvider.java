package com.dolgiy.model.post;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.dolgiy.beans.post.BasicPost;
import com.dolgiy.beans.post.ForumPost;
import com.dolgiy.beans.topic.BasicTopic;
import com.dolgiy.beans.user.User;
import com.dolgiy.beans.user.UserType;
import com.dolgiy.model.AccesException;
import com.dolgiy.model.BasicOperations;
import com.dolgiy.model.topic.TopicOperations;
import com.dolgiy.model.user.UserOperations;

public class ForumPostOperationsProvider extends BasicOperations implements PostOperations {

    private TopicOperations topicOperations;
    private UserOperations userOperations;

    private static final String GET_POSTS = "SELECT idpost AS id, user_id_creator, topic_id, content, post_id_related_post, creation_time, likes, dislikes FROM post WHERE (topic_id = ? AND is_deleted = 0) ORDER BY creation_time ASC LIMIT ?, ?;";
    private static final String ADD_POST = "INSERT INTO post (user_id_creator, topic_id, post_id_related_post, content) VALUES (?, ?, ?, ?);";
    private static final String REMOVE_POST = "UPDATE post SET is_deleted = 1 WHERE (idpost = ?);";
    private static final String POSTS_COUNT = "SELECT COUNT(*) AS count FROM post WHERE (topic_id = ? AND is_deleted = 0);";

    public ForumPostOperationsProvider (Connection connection, TopicOperations topicOperations,
            UserOperations userOperations) {
        super(connection);
        this.topicOperations = topicOperations;
        this.userOperations = userOperations;
    }

    public TopicOperations getTopicOperations () {
        return topicOperations;
    }

    protected UserOperations getUserOperations () {
        return userOperations;
    }

    @Override
    public Collection<BasicPost> get (User user, long topicId) {
        return this.get(user, topicId, 0, Long.MAX_VALUE);
    }

    @Override
    public Collection<BasicPost> get (User user, long topicId, long from, long to) {
        BasicTopic thisTopic = getTopicOperations().get(user, topicId);
        try {
            PreparedStatement getPostsStatement = getConnection().prepareStatement(GET_POSTS);
            getPostsStatement.setLong(1, topicId);
            getPostsStatement.setLong(2, from);
            getPostsStatement.setLong(3, to);
            ResultSet result = getPostsStatement.executeQuery();
            List<BasicPost> postList = new ArrayList<BasicPost>();
            BasicPost post;
            while (result.next()) {
                post = this.<BasicPost>read(result);
                post.setTopic(thisTopic);
                post.setCreator(getUserOperations().get(getValue(result, "user_id_creator")));
                postList.add(post);
            }
            return postList;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void remove (User user, long postId) throws AccesException {
        try {
            PreparedStatement removeStatement = getConnection().prepareStatement(REMOVE_POST);
            removeStatement.setLong(1, postId);
            if (removeStatement.executeUpdate() == 0) {
                throw new SQLException("REMOVE statement didn't affect the database.");
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

    }

    @Override
    public void add (User user, String content, long topicId) throws AccesException {
        this.add(user, content, topicId, -1);

    }

    @Override
    public void add (User user, String content, long topicId, long relatedPost) throws AccesException {
        try {
            PreparedStatement addPostStatement = getConnection().prepareStatement(ADD_POST);
            addPostStatement.setLong(1, user.getId());
            addPostStatement.setLong(2, topicId);
            if (relatedPost < 1) {
                addPostStatement.setNull(3, java.sql.Types.NULL);
            } else {
                addPostStatement.setLong(3, relatedPost);
            }
            addPostStatement.setString(4, content);
            if (addPostStatement.executeUpdate() == 0) {
                throw new SQLException("UPDATE statement didn't affect the database.");
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

    }

    @SuppressWarnings("unchecked")
    @Override
    protected <T> T read (ResultSet... resultSets) throws SQLException {
        ForumPost post = new ForumPost(resultSets[0].getLong("id"), resultSets[0].getDate("creation_time"));
        post.setContent(resultSets[0].getString("content"));
        post.setRelatedPostId(resultSets[0].getLong("post_id_related_post"));
        return (T) post;
    }

    @Override
    public int count (User user, long topicId) {
        try {
            PreparedStatement countPostsStatement = getConnection().prepareStatement(POSTS_COUNT);
            countPostsStatement.setLong(1, topicId);
            ResultSet result = countPostsStatement.executeQuery();
            if (result.next()) {
                return result.getInt("count");
            } else {
                throw new SQLException("Empty result set was returned.");
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

}
