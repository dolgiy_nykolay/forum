package com.dolgiy.model.post;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;

import com.dolgiy.beans.post.BasicPost;
import com.dolgiy.beans.topic.BasicTopic;
import com.dolgiy.beans.user.User;
import com.dolgiy.beans.user.UserType;
import com.dolgiy.model.AccesException;
import com.dolgiy.model.topic.TopicOperations;
import com.dolgiy.model.user.UserOperations;

public class SecurePostOperations extends ForumPostOperationsProvider {

    protected static final String GET_POST_SQL = "SELECT idpost AS id, user_id_creator, topic_id, content, post_id_related_post, creation_time, likes, dislikes FROM post WHERE (idpost = ? AND is_deleted = 0);";

    public SecurePostOperations (Connection connection, TopicOperations topicOperations,
            UserOperations userOperations) {
        super(connection, topicOperations, userOperations);
    }

    @Override
    public Collection<BasicPost> get (User user, long topicId) {
        return this.get(user, topicId, 0, Integer.MAX_VALUE);
    }

    @Override
    public Collection<BasicPost> get (User user, long topicId, long from, long to) {
        BasicTopic topic = getTopicOperations().get(user, topicId);
        if (user.getUserType() == UserType.BasicType.GUEST && topic == null) { // Topic = null when user doesn't have
                                                                               // permission for access it.
            return null;
        } else {
            return super.get(user, topicId, from, to);
        }
    }

    @Override
    public void remove (User user, long postId) throws AccesException {
        try {
            PreparedStatement getPostStatement = getConnection().prepareStatement(GET_POST_SQL);
            getPostStatement.setLong(1, postId);
            ResultSet result = getPostStatement.executeQuery();
            result.next();
            BasicTopic topic = getTopicOperations().get(user, result.getLong("topic_id"));
            if (topic.isModificationAllowed(user.getId())) {
                super.remove(user, postId);
            } else {
                throw new AccesException(
                        "User " + user.getId() + " doesn't have permission for execute this operation.");
            }

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void add (User user, String content, long topicId) throws AccesException {
        this.add(user, content, topicId, -1);
    }

    @Override
    public void add (User user, String content, long topicId, long relatedPost) throws AccesException {
        BasicTopic topic = getTopicOperations().get(user, topicId);
        if (user.getUserType() != UserType.BasicType.GUEST) {
            if (topic.isOpened() || topic.isModificationAllowed(user.getId())) {
                super.add(user, content, topicId, relatedPost);
            } else {
                throw new AccesException(
                        "User " + user.getId() + " doesn't have permission for execute this operation.");
            }
        }
    }

    @Override
    public int count (User user, long topicId) {
        BasicTopic topic = getTopicOperations().get(user, topicId);
        if(!topic.isPrivate() || user.getUserType() != UserType.BasicType.GUEST) {
            return super.count(user, topicId);
        }else {
            return 0;
        }
    }

}
