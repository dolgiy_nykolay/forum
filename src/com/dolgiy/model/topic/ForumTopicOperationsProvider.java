package com.dolgiy.model.topic;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.dolgiy.beans.section.BasicSection;
import com.dolgiy.beans.topic.BasicTopic;
import com.dolgiy.beans.topic.ForumTopic;
import com.dolgiy.beans.user.User;
import com.dolgiy.model.AccesException;
import com.dolgiy.model.BasicOperations;
import com.dolgiy.model.section.SectionOperations;
import com.dolgiy.model.user.UserOperations;

public class ForumTopicOperationsProvider extends BasicOperations implements TopicOperations {

    private final SectionOperations sectionOperations;
    private final UserOperations userOperations;

    protected static final String GET_TOPIC_MODERATORS = "SELECT u.iduser AS id FROM user u JOIN topic t ON (t.idtopic = ?) JOIN permission p ON (p.section_id = t.section_id AND u.iduser = p.user_id_owner_of_permission AND (p.cancel_time > NOW() OR ISNULL(p.cancel_time))) UNION SELECT iduser AS id FROM user WHERE (role = 'ADMIN');";
    protected static final String GET_TOPICS = "SELECT idtopic AS id, name, is_open, user_id_creator, is_private, is_pinned, section_id, creation_time FROM topic WHERE is_deleted = 0 AND (is_private = 0 OR is_private = ?) AND is_pinned = ? AND section_id = ? ORDER BY creation_time DESC LIMIT ?, ?;";
    protected static final String CREATE_TOPIC = "INSERT INTO topic (name, section_id, user_id_creator, is_private, is_pinned) VALUES (?, ?, ?, ?, ?);";
    protected static final String REMOVE_TOPIC = "UPDATE topic SET is_deleted = 1 WHERE (idtopic = ?);";
    protected static final String UPDATE_TOPIC = "UPDATE topic SET name = ?, section_id = ?, is_private = ?, is_open = ?, is_pinned = ? WHERE (idtopic = ?);";
    protected static final String GET_TOPIC = "SELECT idtopic AS id, name, is_open, user_id_creator, is_private, is_pinned, section_id, creation_time FROM topic WHERE is_deleted = 0 AND (is_private = 0 OR is_private = ?)AND idtopic = ?;";

    public ForumTopicOperationsProvider (Connection connection, SectionOperations sectionOperations, UserOperations userOperations) {
        super(connection);
        this.sectionOperations = sectionOperations;
        this.userOperations = userOperations;
    }

    protected SectionOperations getSectionOperations () {
        return sectionOperations;
    }
    
    protected UserOperations getUserOperations() {
        return this.userOperations;
    }

    @Override
    public Iterable<BasicTopic> get (User user, long sectionId, boolean pinned) {
        return get(user, sectionId, pinned, 0, Long.MAX_VALUE);
    }

    @Override
    public Iterable<BasicTopic> get (User user, long sectionId, boolean pinned, long from, long to) {
        return getTopics(user, sectionId, pinned, true, from, to);
    }

    protected Iterable<BasicTopic> getTopics (User user, long sectionId, boolean isPinned, boolean includePrivate, long from,
            long to) {
        try {
            PreparedStatement getTopicsStatement = getConnection().prepareStatement(GET_TOPICS);
            getTopicsStatement.setBoolean(1, includePrivate);
            getTopicsStatement.setBoolean(2, isPinned);
            getTopicsStatement.setLong(3, sectionId);
            getTopicsStatement.setLong(4, from);
            getTopicsStatement.setLong(5, to);
            ResultSet result = getTopicsStatement.executeQuery();
            List<BasicTopic> topicsList = new ArrayList<BasicTopic>();
            PreparedStatement getModeratorsStatement;
            ResultSet moderators;
            BasicSection parentSection = getSectionOperations().get(user, sectionId);
            long topicId;
            BasicTopic topic;
            while (result.next()) {
                getModeratorsStatement = getConnection().prepareStatement(GET_TOPIC_MODERATORS);
                topicId = this.<Long>getValue(result, "id");
                getModeratorsStatement.setLong(1, topicId);
                moderators = getModeratorsStatement.executeQuery();
                topic = this.<BasicTopic>read(result, moderators);
                topic.setParentSection(parentSection);
                topic.setCreator(getUserOperations().get(this.<Long>getValue(result, "user_id_creator")));
                topicsList.add(topic);
            }
            return topicsList;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public BasicTopic get (User user, long topicId) {
        return getTopic(user, topicId, true);
    }

    protected BasicTopic getTopic (User user, long topicId, boolean includePrivate) {
        BasicTopic topic = null;
        try {
            PreparedStatement getTopicStatement = getConnection().prepareStatement(GET_TOPIC);
            getTopicStatement.setBoolean(1, includePrivate);
            getTopicStatement.setLong(2, topicId);
            ResultSet result = getTopicStatement.executeQuery();
            if (result.next()) {
                PreparedStatement getModeratorsStatement = getConnection().prepareStatement(GET_TOPIC_MODERATORS);
                getModeratorsStatement.setLong(1, topicId);
                ResultSet moderators = getModeratorsStatement.executeQuery();
                topic = this.<BasicTopic>read(result, moderators);
                topic.setParentSection(getSectionOperations().get(user, result.getLong("section_id")));
                topic.setCreator(getUserOperations().get(result.getLong("user_id_creator")));
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return topic;
    }

    @Override
    public void add (User user, String name, long sectionId, boolean isPrivate, boolean isPinned)
            throws AccesException {
        try {
            PreparedStatement createTopicStatement = getConnection().prepareStatement(CREATE_TOPIC);
            createTopicStatement.setString(1, name);
            createTopicStatement.setLong(2, sectionId);
            createTopicStatement.setLong(3, user.getId());
            createTopicStatement.setBoolean(4, isPrivate);
            createTopicStatement.setBoolean(5, isPinned);
            if (createTopicStatement.executeUpdate() == 0) {
                throw new SQLException("INSERT statement didn't affect the database.");
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void remove (User user, long topicId) throws AccesException {
        try {
            PreparedStatement deleteStatement = getConnection().prepareStatement(REMOVE_TOPIC);
            deleteStatement.setLong(1, topicId);
            if (deleteStatement.executeUpdate() == 0) {
                throw new SQLException("UPDATE statement didn't affect the database.");
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

    }

    @Override
    public void update (User user, long topicId, long sectionId, String name, boolean isPrivate, boolean isOpen,
            boolean isPinned) throws AccesException {
        // FIXME: This method can modify topic that has been deleted.
        try {
            PreparedStatement updateStatement = getConnection().prepareStatement(UPDATE_TOPIC);
            updateStatement.setString(1, name);
            updateStatement.setLong(2, sectionId);
            updateStatement.setBoolean(3, isPrivate);
            updateStatement.setBoolean(4, isOpen);
            updateStatement.setBoolean(5, isPinned);
            updateStatement.setLong(6, topicId);
            if (updateStatement.executeUpdate() == 0) {
                throw new SQLException("UPDATE statement didn't affect the database.");
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

    }

    

    @SuppressWarnings("unchecked")
    @Override
    protected <T> T read (ResultSet... resultSets) throws SQLException {
        ForumTopic topic = new ForumTopic(resultSets[0].getLong("id"), resultSets[0].getDate("creation_time"));
        topic.setName(resultSets[0].getString("name"));
        topic.setOpened(resultSets[0].getBoolean("is_open"));
        topic.setPinned(resultSets[0].getBoolean("is_pinned"));
        topic.setPrivate(resultSets[0].getBoolean("is_private"));
        while(resultSets[1].next()) {
            topic.addModeratorId(resultSets[1].getLong("id"));
        }
        return (T) topic;
    }

    
}
