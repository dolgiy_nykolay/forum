package com.dolgiy.model.topic;

import com.dolgiy.beans.topic.BasicTopic;
import com.dolgiy.beans.user.User;
import com.dolgiy.model.AccesException;

public interface TopicOperations {

    public Iterable<BasicTopic> get (User user, long sectionId, boolean pinned);

    public Iterable<BasicTopic> get (User user, long sectionId, boolean pinned, long from, long to);

    public BasicTopic get (User user, long topicId);

    public void add (User user, String name, long sectionId, boolean isPrivate, boolean isPinned) throws AccesException;

    public void remove (User user, long topicId) throws AccesException;

    public void update (User user, long topicId, long sectionId, String name, boolean isPrivate, boolean isOpen,
            boolean isPinned) throws AccesException;
}
