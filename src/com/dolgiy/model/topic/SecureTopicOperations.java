package com.dolgiy.model.topic;

import java.sql.Connection;

import com.dolgiy.beans.section.BasicSection;
import com.dolgiy.beans.topic.BasicTopic;
import com.dolgiy.beans.user.User;
import com.dolgiy.beans.user.UserType;
import com.dolgiy.model.AccesException;
import com.dolgiy.model.section.SectionOperations;
import com.dolgiy.model.user.UserOperations;

public class SecureTopicOperations extends ForumTopicOperationsProvider {

    public SecureTopicOperations (Connection connection, SectionOperations sectionOperations, UserOperations userOperations) {
        super(connection, sectionOperations, userOperations);
    }

    @Override
    public Iterable<BasicTopic> get (User user, long sectionId, boolean pinned) {
        return this.get(user, sectionId, pinned, 0, Long.MAX_VALUE);
    }

    @Override
    public Iterable<BasicTopic> get (User user, long sectionId, boolean pinned, long from, long to) {
        return super.getTopics(user, sectionId, pinned, user.getUserType() != UserType.BasicType.GUEST, from, to);
    }

    @Override
    public BasicTopic get (User user, long topicId) {
        return super.getTopic(user, topicId, user.getUserType() != UserType.BasicType.GUEST);
    }

    @Override
    public void add (User user, String name, long sectionId, boolean isPrivate, boolean isPinned)
            throws AccesException {
        if (user.getUserType() != UserType.BasicType.GUEST) {
            BasicSection section = getSectionOperations().get(user, sectionId);
            if (section != null && (section.isOpened() || section.isModificationAllowed(user.getId()))) {
                isPrivate = section.isPrivate() ? section.isPrivate() : isPrivate;
                super.add(user, name, sectionId, isPrivate, isPinned);
            } else {
                throw new AccesException(
                        "User: " + user.getId() + " doesn't have permission for execute this operation.");
            }
        } else {
            throw new AccesException("User: " + user.getId() + " doesn't have permission for execute this operation.");
        }
    }

    @Override
    public void remove (User user, long topicId) throws AccesException {
        BasicTopic thisTopic = get(user, topicId);
        if (thisTopic != null
                && (thisTopic.isModificationAllowed(user.getId()) || user.getId() == thisTopic.getCreator().getId())) {
            super.remove(user, topicId);

        } else {
            throw new AccesException("User: " + user.getId() + " doesn't have permission for execute this operation.");
        }

    }

    @Override
    public void update (User user, long topicId, long sectionId, String name, boolean isPrivate, boolean isOpen,
            boolean isPinned) throws AccesException {
        BasicTopic thisTopic = get(user, topicId);
        if(thisTopic == null) {
            throw new AccesException("User: " + user.getId() + " doesn't have permission for execute this operation.");
        }
        BasicSection currentSection = thisTopic.getParentSection();
        BasicSection newSection;
        if (sectionId == thisTopic.getParentSection().getId()) {
            newSection = currentSection;
        } else {
            newSection = getSectionOperations().get(user, sectionId);
        }
        if (thisTopic.isModificationAllowed(user.getId())) {
            isPrivate = newSection.isPrivate() ? true : isPrivate;
            isOpen = newSection.isOpened() ? isOpen : false;
            super.update(user, topicId, sectionId, name, isPrivate, isOpen, isPinned);
        } else if (thisTopic.getCreator().getId() == user.getId()) {
            isOpen = thisTopic.isOpened() ? isOpen : false; // Creator can close his topic but can't open it if topic
                                                            // has already closed.
            sectionId = thisTopic.getParentSection().getId(); // Creator can't change section that his topic belong to.
            isPinned = thisTopic.isPinned(); // Creator can't pin or unpin his topic.
            isPrivate = currentSection.isPrivate() ? true : isPrivate; // Creator can make section private or non
                                                                       // private, but if only section is not private.
                                                                       // In private sections can be only private
                                                                       // topics.
            super.update(user, topicId, sectionId, name, isPrivate, isOpen, isPinned);
        } else {
            throw new AccesException("User: " + user.getId() + " doesn't have permission for execute this operation.");
        }
    }

}
