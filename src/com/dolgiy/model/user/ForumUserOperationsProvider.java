package com.dolgiy.model.user;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.dolgiy.beans.user.ForumUser;
import com.dolgiy.beans.user.User;
import com.dolgiy.beans.user.UserType;
import com.dolgiy.beans.user.block.BlockedException;
import com.dolgiy.beans.user.block.ForumUserBlock;
import com.dolgiy.beans.user.permission.Permission;
import com.dolgiy.beans.user.permission.UserPermission;
import com.dolgiy.model.AccesException;
import com.dolgiy.model.BasicOperations;
import com.dolgiy.model.section.SectionOperations;

import sun.security.action.GetBooleanSecurityPropertyAction;

/**
 * 
 * Represents operations with database for users. Also keep the SQL requests for
 * making this operations.
 *
 */
public class ForumUserOperationsProvider extends BasicOperations implements UserOperations {

    protected static final long SESSION_ACTIVE_TIME = 3600; // 1 hour = 3600 seconds.
    protected static final String GET_USER_BY_CREDENTIALS_SQL = "SELECT iduser, name,surname,nickname,current_profile_photo,email,role,likes,dislikes FROM user WHERE ( login=? AND password=? )";
    protected static final String CREATE_SESSION_SQL = "INSERT INTO active_session (app_session_id, user_id, ip_address, expire_time) VALUES (?, ?, ?, NOW() + INTERVAL ? SECOND)";
    protected static final String GET_USER_BY_SESSION_SQL = "SELECT a.idactive_session, u.iduser,u.name,u.surname,u.nickname,u.current_profile_photo,u.email,u.role,u.likes,u.dislikes FROM active_session AS a JOIN (user AS u) ON (a.user_id = u.iduser) WHERE (a.app_session_id = ? AND a.ip_address = ? AND a.expire_time > NOW())";
    protected static final String EXTEND_SESSION_TIME_SQL = "UPDATE active_session SET expire_time = NOW() + INTERVAL ? SECOND, last_access_time = NOW() WHERE idactive_session = ?";
    protected static final String CLOSE_SESSION_SQL = "UPDATE active_session SET expire_time = NOW() WHERE app_session_id = ? AND ip_address = ? AND user_id = ?";
    protected static final String ADD_USER_SQL = "INSERT INTO user (name,surname,nickname,email,login,password) VALUES (?, ?, ?, ?, ?, ?)";
    protected static final String GET_USER_BY_LOGIN_SQL = "SELECT login FROM user WHERE (login = ?)";
    protected static final String GET_USER_BY_EMAIL_SQL = "SELECT email FROM user WHERE (email = ?)";
    protected static final String GET_USER_BY_ID = "SELECT iduser, name, surname, nickname, current_profile_photo, email, role, likes, dislikes FROM user WHERE (iduser = ?);";
    protected static final String SET_PROFILE_PHOTO_SQL = "UPDATE user SET current_profile_photo = ? WHERE (iduser = ?);";
    protected static final String SET_PASSWORD_SQL = "UPDATE user SET password = ? WHERE (iduser = ?);";
    protected static final String BLOCK_USER_SQL = "INSERT INTO users_ban (user_id, user_id_executor, message, block_expire_date) VALUES (?, ?, ?, ?);";
    protected static final String GET_ACTIVE_BLOCKS_SQL = "SELECT * FROM db_forum.users_ban WHERE (user_id = ? AND block_expire_date < NOW());";
    protected static final String SET_USER_ROLE_SQL = "UPDATE user SET role = ? WHERE (iduser = ?);";
    protected static final String DELETE_PERMISSIONS_SQL = "UPDATE permission SET cancel_time = NOW(), user_id_canceler = ? WHERE (user_id_owner_of_permission = ?);";
    protected static final String ADD_PERMISSION_SQL = "INSERT INTO permission (user_id_owner_of_permission, section_id, user_id_creator) VALUES (?, ?, ?);";
    protected static final String DELETE_PERMISSION_SQL = "UPDATE permission SET cancel_time = NOW(), user_id_canceler = ? WHERE (user_id_owner_of_permission = ? AND section_id = ?);";
    protected static final String GET_PERMISSIONS_SQL = "SELECT * FROM permission WHERE (user_id_owner_of_permission = ? AND (ISNULL(cancel_time) OR cancel_time > NOW()));";
    
    
    private MessageDigest messageDigest;
    private SectionOperations sectionOperations;
    
    protected SectionOperations getSectionOperations() {
        return this.sectionOperations;
    }
    
    
    protected MessageDigest getMessageDigest () {
        return messageDigest;
    }

    /**
     * Constructs new object with given parameters. Also it use as DigestAlgorithm
     * SHA-256 algorithm.
     * 
     * @param connection
     *            Gets the connection to database.
     */
    public ForumUserOperationsProvider (Connection connection, SectionOperations sectionOperations) {
        super(connection);
        this.sectionOperations = sectionOperations;
        try {
            this.messageDigest = MessageDigest.getInstance("SHA-256");
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Constructs new object with given parameters.
     * 
     * @param connection
     *            Gets the connection to database.
     * @param heshingAlgorithmName
     *            The name of digest algorithm for storing password in database.
     * @throws NoSuchAlgorithmException
     *             If given algorithm doesn't exists.
     */
    public ForumUserOperationsProvider (Connection connection, SectionOperations sectionOperations, String heshingAlgorithmName)
            throws NoSuchAlgorithmException {
        super(connection);
        this.sectionOperations = sectionOperations;
        this.messageDigest = MessageDigest.getInstance(heshingAlgorithmName);
    }

    /**
     * Constructs new object with given parameters.
     * 
     * @param connection
     *            Gets the connection to database.
     * @param messageDigest
     *            Digest algorithm for storing password in database.
     */
    public ForumUserOperationsProvider (Connection connection, SectionOperations sectionOperations, MessageDigest messageDigest) {
        super(connection);
        this.sectionOperations = sectionOperations;
        this.messageDigest = messageDigest;
    }

    /**
     * Tries to get User object by session Id.
     * 
     * @param sessionId
     *            Gets the session Id that assigned to this request.
     * @param ipAddress
     *            Gets the IP address of request.
     * @param resultReader
     *            object that can read result and creates {@link User} object.
     * @return User object contained information about user.
     * @throws SessionException
     * @throws BlockedException
     */
    public User signIn (String sessionId, String ipAddress) throws SessionException, BlockedException {

        User user = new ForumUser();
        try {
            PreparedStatement checkSessionStatement = getConnection().prepareStatement(GET_USER_BY_SESSION_SQL);
            checkSessionStatement.setString(1, sessionId);
            checkSessionStatement.setString(2, ipAddress);
            ResultSet result = checkSessionStatement.executeQuery();
            if (result.next()) {
                user = this.<User>read(result);

                PreparedStatement getBlocksStatement = getConnection().prepareStatement(GET_ACTIVE_BLOCKS_SQL);
                getBlocksStatement.setLong(1, user.getId());
                ResultSet blocks = getBlocksStatement.executeQuery();
                while (blocks.next()) {
                    user.addBlock(new ForumUserBlock(blocks.getString("message"),
                            this.get(blocks.getLong("user_id_executor")), blocks.getDate("block_expire_date")));
                }
                if (user.getActiveBlocks().size() > 0) {
                    throw new BlockedException(user.getActiveBlocks());
                } else {
                    PreparedStatement extendSessionStatement = getConnection()
                            .prepareStatement(EXTEND_SESSION_TIME_SQL);
                    extendSessionStatement.setLong(1, SESSION_ACTIVE_TIME);
                    extendSessionStatement.setInt(2, result.getInt("idactive_session"));

                    if (extendSessionStatement.executeUpdate() == 0) {
                        throw new SessionException("Cant extend session for existing user: " + user.getId());
                    }
                }

            }

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return user;
    }

    /**
     * Tries to get user by login and password and open session for this user.
     * 
     * @param login
     *            Gets the user's login.
     * @param password
     *            Gets the user's password.
     * @param sessionId
     *            Gets the session Id that assigned to this request.
     * @param ipAddress
     *            Gets the IP address of request.
     * @param resultReader
     *            object that can read result and creates {@link User} object.
     * @return User object contained information about user.
     * @throws AuthenticationException
     *             If there aren't user with given login and password in database.
     * @throws SessionException
     *             If some error occurred while creating a session for existing
     *             user.
     * @throws BlockedException
     */
    public User signIn (String login, String password, String sessionId, String ipAddress)
            throws AuthenticationException, SessionException, BlockedException {

        User user = null;
        try {
            PreparedStatement authQuery = getConnection().prepareStatement(GET_USER_BY_CREDENTIALS_SQL);
            authQuery.setString(1, login);
            authQuery.setString(2, new String(messageDigest.digest(password.getBytes())));
            ResultSet result = authQuery.executeQuery();
            if (result.next()) { // login is unique in database, so if we have 'next' we have only one.
                user = this.<User>read(result);

                PreparedStatement getBlocksStatement = getConnection().prepareStatement(GET_ACTIVE_BLOCKS_SQL);
                getBlocksStatement.setLong(1, user.getId());
                ResultSet blocks = getBlocksStatement.executeQuery();
                while (blocks.next()) {
                    user.addBlock(new ForumUserBlock(blocks.getString("message"),
                            this.get(blocks.getLong("user_id_executor")), blocks.getDate("block_expire_date")));
                }
                if (user.getActiveBlocks().size() > 0) {
                    throw new BlockedException(user.getActiveBlocks());
                } else {
                    PreparedStatement createSessionStatement = getConnection().prepareStatement(CREATE_SESSION_SQL);
                    createSessionStatement.setString(1, sessionId);
                    createSessionStatement.setLong(2, user.getId());
                    createSessionStatement.setString(3, ipAddress);
                    createSessionStatement.setLong(4, SESSION_ACTIVE_TIME);

                    if (createSessionStatement.executeUpdate() == 0) {
                        throw new SessionException("Error while creating new session for user: " + user.getId());
                    }
                }

            } else {
                throw new AuthenticationException("Cant find user with provided credentials.");
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        return user;
    }

    /**
     * Puts the data about new user in database.
     * 
     * @param name
     *            Gets the user's name.
     * @param surname
     *            Gets the user's surname.
     * @param nickname
     *            Gets the user's nickname.
     * @param email
     *            Gets the user's email.
     * @param login
     *            Gets the user's login.
     * @param password
     *            Gets the user's password.
     * @throws LoginExistsException
     *             If user with given login exists.
     * @throws EmailExistsException
     *             If user with given email exists.
     */
    public void signUp (String name, String surname, String nickname, String email, String login, String password)
            throws LoginExistsException, EmailExistsException {
        if (checkLoginExisting(login)) {
            throw new LoginExistsException("User with given login exists.");
        }
        if (checkEmailExisting(email)) {
            throw new EmailExistsException("User with given email exists.");
        }
        try {
            PreparedStatement registerUserStatement = getConnection().prepareStatement(ADD_USER_SQL);
            registerUserStatement.setString(1, name);
            registerUserStatement.setString(2, surname);
            registerUserStatement.setString(3, nickname);
            registerUserStatement.setString(4, email);
            registerUserStatement.setString(5, login);
            registerUserStatement.setString(6, new String(messageDigest.digest(password.getBytes())));
            registerUserStatement.executeUpdate();

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Checks the existing of given email in database.
     * 
     * @param email
     *            Gets the user's email.
     * @return true if exists, and false otherwise.
     */
    public boolean checkEmailExisting (String email) {
        try {
            PreparedStatement checkUserExistingStatement = getConnection().prepareStatement(GET_USER_BY_EMAIL_SQL);
            checkUserExistingStatement.setString(1, email);
            ResultSet result = checkUserExistingStatement.executeQuery();

            return result.next(); // User exists if we have at least one row in ResultSet.

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Checks the existing of given login in database.
     * 
     * @param login
     *            Gets the user's login.
     * @return true if exists, and false otherwise.
     */
    public boolean checkLoginExisting (String login) {
        try {
            PreparedStatement checkUserExistingStatement = getConnection().prepareStatement(GET_USER_BY_LOGIN_SQL);
            checkUserExistingStatement.setString(1, login);
            ResultSet result = checkUserExistingStatement.executeQuery();

            return result.next(); // User exists if we have at least one row in ResultSet.

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Sets the session expire time for NOW(), this makes session unavailable for
     * using.
     * 
     * @param user
     *            Gets the User object that contains user Id.
     * @param sessionId
     *            Gets the unique session identifier.
     * @param ipAddres
     *            Gets the IP Address of request.
     */
    public void signOut (User user, String sessionId, String ipAddres) {
        try {
            PreparedStatement closeSessionStatement = getConnection().prepareStatement(CLOSE_SESSION_SQL);
            closeSessionStatement.setString(1, sessionId);
            closeSessionStatement.setString(2, ipAddres);
            closeSessionStatement.setLong(3, user.getId());
            closeSessionStatement.executeUpdate();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    protected <T> T read (ResultSet... resultSets) throws SQLException {
        ForumUser user = new ForumUser(resultSets[0].getLong("iduser"),
                UserType.BasicType.valueOf(resultSets[0].getString("role")));
        user.setName(resultSets[0].getString("name"));
        user.setSurname(resultSets[0].getString("surname"));
        user.setNickname(resultSets[0].getString("nickname"));
        user.setCurrentProfilePhotoId(resultSets[0].getLong("current_profile_photo"));
        user.setEmail(resultSets[0].getString("email"));
        user.setLikes(resultSets[0].getInt("likes"));
        user.setDislikes(resultSets[0].getInt("dislikes"));
        return (T) user;
    }

    @Override
    public User get (long userId) {
        try {
            PreparedStatement getUserStatement = getConnection().prepareStatement(GET_USER_BY_ID);
            getUserStatement.setLong(1, userId);
            ResultSet result = getUserStatement.executeQuery();
            User user = null;
            if (result.next()) {
                user = read(result);
                PreparedStatement getPermissionsStatement = getConnection().prepareStatement(GET_PERMISSIONS_SQL);
                getPermissionsStatement.setLong(1, user.getId());
                ResultSet permissions = getPermissionsStatement.executeQuery();
                UserPermission p;
                while(permissions.next()) {
                    p = new UserPermission(getSectionOperations().get(user, permissions.getLong("section_id")),user);
                    p.setCreationDate(permissions.getDate("creation_time"));
                    p.setDeactivationDate(permissions.getDate("cancel_time"));
                    p.setCreator(get(permissions.getLong("user_id_creator")));
                    p.setDeactivator(get(permissions.getLong("user_id_canceler")));
                    user.addPermission(p);
                }
            }
            return user;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void setProfilePhoto (User commandExecutor, long userId, long imageId) throws AccesException {
        try {
            PreparedStatement setPhotoStatement = getConnection().prepareStatement(SET_PROFILE_PHOTO_SQL);
            setPhotoStatement.setLong(1, imageId);
            setPhotoStatement.setLong(2, userId);
            if (setPhotoStatement.executeUpdate() == 0) {
                throw new SQLException("UPDATE statement didn't affect the database.");
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

    }

    @Override
    public void setPassword (User commandExecutor, long userId, String oldPassword, String newPassword)
            throws AccesException {
        try {
            PreparedStatement setPasswordStatement = getConnection().prepareStatement(SET_PASSWORD_SQL);
            setPasswordStatement.setString(1, new String(messageDigest.digest(newPassword.getBytes())));
            setPasswordStatement.setLong(2, userId);
            if (setPasswordStatement.executeUpdate() == 0) {
                throw new SQLException("UPDATE statement didn't affect the database.");
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

    }

    @Override
    public void blockUser (User commandExecutor, long userId, String cause, Date blockExpireDate)
            throws AccesException {
        try {
            PreparedStatement blockUserStatement = getConnection().prepareStatement(BLOCK_USER_SQL);
            blockUserStatement.setLong(1, userId);
            blockUserStatement.setLong(2, commandExecutor.getId());
            blockUserStatement.setString(3, cause);
            blockUserStatement.setDate(4, blockExpireDate);
            if (blockUserStatement.executeUpdate() == 0) {
                throw new SQLException("INSERT statement didn't affect the database.");
            }

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

    }

    @Override
    public void setRole (User commandExecutor, long userId, UserType type) throws AccesException {
        try {
            if (type == UserType.BasicType.USER) {
                PreparedStatement deletePermissionsStatement = getConnection().prepareStatement(DELETE_PERMISSIONS_SQL);
                deletePermissionsStatement.setLong(1, commandExecutor.getId());
                deletePermissionsStatement.setLong(2, userId);
                deletePermissionsStatement.executeUpdate();
            }
            PreparedStatement setUserRoleStatement = getConnection().prepareStatement(SET_USER_ROLE_SQL);
            setUserRoleStatement.setString(1, type.toString());
            setUserRoleStatement.setLong(2, userId);
            if (setUserRoleStatement.executeUpdate() == 0) {
                throw new SQLException("UPDATE statement didn't affect the database.");
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

    }

    @Override
    public void addPermission (User commandExecutor, long userId, long sectionId) throws AccesException {
        try {
            PreparedStatement addPermissionStatement = getConnection().prepareStatement(ADD_PERMISSION_SQL);
            addPermissionStatement.setLong(1, userId);
            addPermissionStatement.setLong(2, sectionId);
            addPermissionStatement.setLong(3, commandExecutor.getId());
            if (addPermissionStatement.executeUpdate() == 0) {
                throw new SQLException("INSERT statement didn't affect the database.");
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

    }

    @Override
    public void removePermission (User commandExecutor, long userId, long sectionId) throws AccesException {
        try {
            PreparedStatement deletePermissionStatement = getConnection().prepareStatement(DELETE_PERMISSION_SQL);
            deletePermissionStatement.setLong(1, commandExecutor.getId());
            deletePermissionStatement.setLong(2, userId);
            deletePermissionStatement.setLong(3, sectionId);
            if(deletePermissionStatement.executeUpdate() == 0) {
                throw new SQLException("UPDATE statement didn't affect the database.");
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

    }

}
