package com.dolgiy.model.user;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.dolgiy.beans.user.User;
import com.dolgiy.beans.user.UserType;
import com.dolgiy.model.AccesException;
import com.dolgiy.model.section.SectionOperations;

public class SecureUserOperations extends ForumUserOperationsProvider {

    protected static final String CHECK_PASSWORD_SQL = "SELECT iduser FROM user WHERE (iduser = ? AND password = ?);";

    public SecureUserOperations (Connection connection, SectionOperations sectionOperations, MessageDigest messageDigest) {
        super(connection, sectionOperations, messageDigest);
    }

    public SecureUserOperations (Connection connection, SectionOperations sectionOperations, String heshingAlgorithmName) throws NoSuchAlgorithmException {
        super(connection, sectionOperations, heshingAlgorithmName);
    }

    public SecureUserOperations (Connection connection, SectionOperations sectionOperations) {
        super(connection, sectionOperations);
    }

    @Override
    public void setProfilePhoto (User commandExecutor, long userId, long imageId) throws AccesException {
        if (commandExecutor.getId() == userId) {
            super.setProfilePhoto(commandExecutor, userId, imageId);
        } else {
            throw new AccesException(
                    "User " + commandExecutor.getId() + " doesn't have permission to execute this operation");
        }

    }

    @Override
    public void setPassword (User commandExecutor, long userId, String oldPassword, String newPassword)
            throws AccesException {
        if (commandExecutor.getId() == userId) {
            try {
                PreparedStatement checkPasswordStatement = getConnection().prepareStatement(CHECK_PASSWORD_SQL);
                checkPasswordStatement.setLong(1, userId);
                checkPasswordStatement.setString(2, new String(getMessageDigest().digest(oldPassword.getBytes())));
                ResultSet result = checkPasswordStatement.executeQuery();
                if (result.next()) {
                    super.setPassword(commandExecutor, userId, oldPassword, newPassword);
                } else {
                    throw new AccesException("Old password doen't match.");
                }
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
        }
    }

    @Override
    public void blockUser (User commandExecutor, long userId, String cause, Date blockExpireDate)
            throws AccesException {
        if (commandExecutor.getUserType() == UserType.BasicType.ADMIN) {
            super.blockUser(commandExecutor, userId, cause, blockExpireDate);
        } else if (commandExecutor.getUserType() == UserType.BasicType.MODERATOR) {
            User underBan = get(userId);
            if (underBan.getUserType() != UserType.BasicType.ADMIN
                    && underBan.getUserType() != UserType.BasicType.MODERATOR) {
                super.blockUser(commandExecutor, userId, cause, blockExpireDate);
            } else {
                throw new AccesException(
                        "User " + commandExecutor.getId() + " doesn't have permission to execute this operation");
            }
        }

    }

    @Override
    public void setRole (User commandExecutor, long userId, UserType type) throws AccesException {
        if (commandExecutor.getUserType() == UserType.BasicType.ADMIN) {
            if (commandExecutor.getId() != userId) {
                super.setRole(commandExecutor, userId, type);
            }
        } else {
            throw new AccesException(
                    "User " + commandExecutor.getId() + " doesn't have permission to execute this operation.");
        }
    }

    @Override
    public void addPermission (User commandExecutor, long userId, long sectionId) throws AccesException {
        if (commandExecutor.getUserType() == UserType.BasicType.ADMIN) {
            User user = get(userId);
            if (user.getUserType() == UserType.BasicType.MODERATOR) {
                super.addPermission(commandExecutor, userId, sectionId);
            }
        } else {
            throw new AccesException(
                    "User " + commandExecutor.getId() + " doesn't have permission to execute this operation.");
        }
    }

    @Override
    public void removePermission (User commandExecutor, long userId, long sectionId) throws AccesException {
        if (commandExecutor.getUserType() == UserType.BasicType.ADMIN) {
            super.removePermission(commandExecutor, userId, sectionId);
        } else {
            throw new AccesException(
                    "User " + commandExecutor.getId() + " doesn't have permission to execute this operation.");
        }

    }

}
