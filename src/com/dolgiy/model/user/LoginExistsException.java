package com.dolgiy.model.user;

public class LoginExistsException extends Exception {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    public LoginExistsException () {
        super();
    }

    public LoginExistsException (String message, Throwable cause, boolean enableSuppression,
            boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

    public LoginExistsException (String message, Throwable cause) {
        super(message, cause);
    }

    public LoginExistsException (String message) {
        super(message);
    }

    public LoginExistsException (Throwable cause) {
        super(cause);
    }

}
