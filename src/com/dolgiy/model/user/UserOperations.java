package com.dolgiy.model.user;

import java.sql.Date;

import com.dolgiy.beans.user.User;
import com.dolgiy.beans.user.UserType;
import com.dolgiy.beans.user.block.BlockedException;
import com.dolgiy.model.AccesException;

/**
 * 
 * Object that can provide basic operation for {@see User}
 *
 */
public interface UserOperations {

    /**
     * Tries to get User object by session Id.
     * 
     * @param sessionId
     *            Gets the session Id that assigned to this request.
     * @param ipAddress
     *            Gets the IP address of request.
     * @return User object contained information about user.
     * @throws SessionException
     */
    public User signIn (String sessionId, String ipAddress) throws SessionException, BlockedException;

    /**
     * Tries to get user by login and password and open session for this user.
     * 
     * @param login
     *            Gets the user's login.
     * @param password
     *            Gets the user's password.
     * @param sessionId
     *            Gets the session Id that assigned to this request.
     * @param ipAddress
     *            Gets the IP address of request.
     * @return User object contained information about user.
     * @throws AuthenticationException
     *             If there aren't user with given login and password in database.
     * @throws SessionException
     *             If some error occurred while creating a session for existing
     *             user.
     */
    public User signIn (String login, String password, String sessionId, String ipAddress)
            throws AuthenticationException, SessionException, BlockedException ;

    /**
     * Puts the data about new user in database.
     * 
     * @param name
     *            Gets the user's name.
     * @param surname
     *            Gets the user's surname.
     * @param nickname
     *            Gets the user's nickname.
     * @param email
     *            Gets the user's email.
     * @param login
     *            Gets the user's login.
     * @param password
     *            Gets the user's password.
     * @throws LoginExistsException
     *             If user with given login exists.
     * @throws EmailExistsException
     *             If user with given email exists.
     */
    public void signUp (String name, String surname, String nickname, String email, String login, String password)
            throws LoginExistsException, EmailExistsException;

    /**
     * Checks the existing of given email in database.
     * 
     * @param email
     *            Gets the user's email.
     * @return true if exists, and false otherwise.
     */
    public boolean checkEmailExisting (String email);

    /**
     * Checks the existing of given login in database.
     * 
     * @param login
     *            Gets the user's login.
     * @return true if exists, and false otherwise.
     */
    public boolean checkLoginExisting (String login);

    /**
     * Sets the session expire time for NOW(), this makes session unavailable for
     * using.
     * 
     * @param user
     *            Gets the User object that contains user Id.
     * @param sessionId
     *            Gets the unique session identifier.
     * @param ipAddres
     *            Gets the IP Address of request.
     */
    public void signOut (User user, String sessionId, String ipAddres);
    
    /**
     * 
     * @param userId
     * @return
     */
    public User get(long userId);
    
    public void setProfilePhoto(User commandExecutor, long userId, long ImageId) throws AccesException;
    
    public void setPassword(User commandExecutor, long userId, String oldPassword, String newPassword) throws AccesException;
    
    public void blockUser(User commandExecutor, long userId, String cause, Date blockExpireDate) throws AccesException;
    
    public void setRole(User commandExecutor, long userId, UserType type) throws AccesException;
    
    public void addPermission(User commandExecutor, long userId, long sectionId) throws AccesException;
    
    public void removePermission(User commandExecutor, long userId, long sectionId) throws AccesException;
}
