package com.dolgiy.model.image;

import java.io.InputStream;

import com.dolgiy.beans.image.Image;
import com.dolgiy.beans.user.User;
import com.dolgiy.model.AccesException;

public interface ImageOperations {

    long add(User user, InputStream image, String mimeType) throws AccesException;
    
    Image get(User user, long id) throws AccesException;
    
}
