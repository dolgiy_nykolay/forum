package com.dolgiy.model.image;

import java.io.InputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.dolgiy.beans.image.ForumImage;
import com.dolgiy.beans.image.Image;
import com.dolgiy.beans.user.User;
import com.dolgiy.model.AccesException;
import com.dolgiy.model.BasicOperations;
import com.mysql.cj.xdevapi.Statement;

public class ImageOperationsImpl extends BasicOperations implements ImageOperations {

    private static final String ADD_IMAGE = "INSERT INTO image (image, user_id_uploader, mime_type) VALUES (?, ?, ?);";
    private static final String GET_IMAGE = "SELECT * FROM image WHERE (id = ?)";

    public ImageOperationsImpl (Connection connection) {
        super(connection);
    }

    @Override
    public long add (User user, InputStream image, String mimeType) throws AccesException{
        try {
            PreparedStatement addImageStatement = getConnection().prepareStatement(ADD_IMAGE, new String[] { "id" });
            addImageStatement.setBlob(1, image);
            addImageStatement.setLong(2, user.getId());
            addImageStatement.setString(3, mimeType);
            if (addImageStatement.executeUpdate() == 0) {
                throw new SQLException("INSERT statement didn't affect the database.");
            }
            ResultSet result = addImageStatement.getGeneratedKeys();
            if (!result.next()) {
                throw new SQLException("INSERT statement didn't affect the database.");
            }
            return result.getLong("GENERATED_KEY");
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public Image get (User user, long id) throws AccesException{
        try {
            PreparedStatement getImageStatement = getConnection().prepareStatement(GET_IMAGE);
            getImageStatement.setLong(1, id);
            ResultSet result = getImageStatement.executeQuery();
            result.next();
            return this.read(result);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    protected <T> T read (ResultSet... resultSets) throws SQLException {
        ForumImage image = new ForumImage(resultSets[0].getBinaryStream("image"), resultSets[0].getString("mime_type"));
        // TODO: Add user reading.
        return (T) image;
    }
}
