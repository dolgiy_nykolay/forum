package com.dolgiy.model.image;

import java.io.InputStream;
import java.sql.Connection;

import com.dolgiy.beans.image.Image;
import com.dolgiy.beans.user.User;
import com.dolgiy.beans.user.UserType;
import com.dolgiy.model.AccesException;

public class SecureImageOperations extends ImageOperationsImpl {

    public SecureImageOperations (Connection connection) {
        super(connection);
    }

    @Override
    public long add (User user, InputStream image, String mimeType) throws AccesException {
        if (user.getUserType() != UserType.BasicType.GUEST) {
            return super.add(user, image, mimeType);
        } else {
            throw new AccesException("User " + user.getId() + "doesn't have prmission for execute this operation.");
        }
    }

    @Override
    public Image get (User user, long id) throws AccesException {

        return super.get(user, id);

    }

}
