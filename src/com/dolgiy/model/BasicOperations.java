package com.dolgiy.model;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * 
 * 
 *
 */
public abstract class BasicOperations {

    private Connection connection;

    /**
     * 
     * @param connection
     */
    public BasicOperations (Connection connection) {
        this.connection = connection;
    }

    /**
     * 
     * @return
     */
    protected Connection getConnection () {
        return connection;
    }

    /**
     * 
     * @param connection
     */
    protected void setConnection (Connection connection) {
        this.connection = connection;
    }

    @SuppressWarnings("unchecked")
    protected <T> T getValue (ResultSet resultSet, String key) throws SQLException {
        return (T) resultSet.getObject(key);
    }

    @SuppressWarnings("unchecked")
    protected <T> List<T> getValues (ResultSet resultSet, String key) throws SQLException {
        List<T> result = new ArrayList<T>();
        while (resultSet.next()) {
            result.add((T) resultSet.getObject(key));
        }
        return Collections.<T>unmodifiableList(result);
    }

    protected abstract <T> T read(ResultSet... resultSets) throws SQLException;
}
