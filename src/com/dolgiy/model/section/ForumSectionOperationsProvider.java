package com.dolgiy.model.section;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.dolgiy.beans.section.BasicSection;
import com.dolgiy.beans.section.ForumSection;
import com.dolgiy.beans.user.User;
import com.dolgiy.model.AccesException;
import com.dolgiy.model.BasicOperations;

/**
 * 
 * Provides operations for sections in a forum.
 *
 */
public class ForumSectionOperationsProvider extends BasicOperations implements SectionOperations {
    
    protected static final String GET_SECTIONS = "SELECT idsection AS id, name, is_open, user_id_creator, is_private, is_pinned, creation_time FROM section WHERE is_deleted = 0 AND is_pinned = ? AND (is_private = 0 OR is_private = ?) ORDER BY creation_time DESC LIMIT ?, ?;";
    protected static final String GET_SECTION_BY_ID = "SELECT idsection AS id, name, is_open, user_id_creator, is_private, is_pinned, creation_time FROM section WHERE (idsection = ? AND (is_private = 0 OR is_private = ?));";
    protected static final String CREATE_SECTION = "INSERT INTO section (name, user_id_creator, is_private, is_pinned) VALUES (?, ?, ?, ?);";
    protected static final String REMOVE_SECTION = "UPDATE section SET is_deleted = 1 WHERE idsection = ?;";
    protected static final String UPDATE_SECTION = "UPDATE section SET name = ?, is_pinned = ?, is_private = ?, is_open = ? WHERE idsection = ?;";
    protected static final String GET_SECTION_MODERATORS = "SELECT DISTINCT u.iduser AS id FROM user u JOIN permission p WHERE ((u.iduser = p.user_id_owner_of_permission AND p.section_id = ? AND (p.cancel_time > NOW() OR ISNULL(p.cancel_time))) OR u.role = 'ADMIN');";

    public ForumSectionOperationsProvider (Connection connection) {
        super(connection);
    }

    @Override
    public Collection<BasicSection> get (User user, boolean pinned) {
        return get(user, pinned, 0, Long.MAX_VALUE);
    }

    @Override
    public Collection<BasicSection> get (User user, boolean pinned, long from, long to) {
        return getSections(pinned, true, from, to);
    }

    @Override
    public BasicSection get (User user, long sectionId) {
        return this.getSection(user, sectionId, true);
    }

    protected BasicSection getSection(User user, long sectionId, boolean includePrivate) {
        BasicSection section = null;
        try {
            PreparedStatement getSectionStatement = getConnection().prepareStatement(GET_SECTION_BY_ID);
            getSectionStatement.setLong(1, sectionId);
            getSectionStatement.setBoolean(2, includePrivate);
            ResultSet result = getSectionStatement.executeQuery();
            if (result.next()) {
                PreparedStatement getModeratorsStatement = getConnection().prepareStatement(GET_SECTION_MODERATORS);
                getModeratorsStatement.setLong(1, sectionId);
                ResultSet moderators = getModeratorsStatement.executeQuery();
                section = this.<BasicSection>read(result, moderators);
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return section;
    }
    
    protected Collection<BasicSection> getSections (boolean isPinned, boolean includePrivate, long from, long to) {
        try {
            PreparedStatement getSections = getConnection().prepareStatement(GET_SECTIONS);
            getSections.setBoolean(1, isPinned);
            getSections.setBoolean(2, includePrivate);
            getSections.setLong(3, from);
            getSections.setLong(4, to);
            ResultSet result = getSections.executeQuery();
            List<BasicSection> sections = new ArrayList<BasicSection>();
            PreparedStatement getModeratorsStatement;
            ResultSet moderators;
            while (result.next()) {
                getModeratorsStatement = getConnection().prepareStatement(GET_SECTION_MODERATORS);
                getModeratorsStatement.setLong(1, getValue(result, "id"));
                moderators = getModeratorsStatement.executeQuery();
                sections.add(this.<BasicSection>read(result, moderators));
            }
            return sections;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void add (User user, String name, boolean isPrivate, boolean isPinned) throws AccesException {
        try {
            PreparedStatement insertStatement = getConnection().prepareStatement(CREATE_SECTION);
            insertStatement.setString(1, name);
            insertStatement.setLong(2, user.getId());
            insertStatement.setBoolean(3, isPrivate);
            insertStatement.setBoolean(4, isPinned);
            if (insertStatement.executeUpdate() == 0) {
                throw new SQLException("INSERT statement didn't affect the database.");
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void remove (User user, long sectionId) throws AccesException {
        try {
            PreparedStatement deleteStatement = getConnection().prepareStatement(REMOVE_SECTION);
            deleteStatement.setLong(1, sectionId);
            if (deleteStatement.executeUpdate() == 0) {
                throw new SQLException("UPDATE statement didn't affect the database.");
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void update (User user, long sectionId, String name, boolean isPrivate, boolean isPinned, boolean isOpened)
            throws AccesException {
        try {
            // FIXME: This method can modify section that has been deleted.
            PreparedStatement updateStatement = getConnection().prepareStatement(UPDATE_SECTION);
            updateStatement.setString(1, name);
            updateStatement.setBoolean(2, isPinned);
            updateStatement.setBoolean(3, isPrivate);
            updateStatement.setBoolean(4, isOpened);
            updateStatement.setLong(5, sectionId);
            if (updateStatement.executeUpdate() == 0) {
                throw new SQLException("UPDATE statement didn't affect the database.");
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

    }

    @SuppressWarnings("unchecked")
    @Override
    protected <T> T read (ResultSet... resultSets) throws SQLException {
        ForumSection section = new ForumSection(resultSets[0].getLong("id"), resultSets[0].getDate("creation_time"));
        section.setName(resultSets[0].getString("name"));
        section.setCreatorId(resultSets[0].getLong("user_id_creator"));
        section.setOpened(resultSets[0].getBoolean("is_open"));
        section.setPinned(resultSets[0].getBoolean("is_pinned"));
        section.setPrivate(resultSets[0].getBoolean("is_private"));
        
        while(resultSets[1].next()) {
            section.addModeratorId(resultSets[1].getLong("id"));
        }
        
        return (T) section;
    }
}
