package com.dolgiy.model.section;

import java.util.Collection;

import com.dolgiy.beans.section.BasicSection;
import com.dolgiy.beans.user.User;
import com.dolgiy.model.AccesException;

public interface SectionOperations {
    
    /**
     * Makes request to database and selects sections from it.
     * 
     * @param pinned
     *            if flag is true method returns pinned sections, otherwise non
     *            pinned.
     * @param user 
     * @return list of sections from the database.
     */
    public Collection<BasicSection> get (User user, boolean pinned);

    /**
     * 
     * @param from
     *            value used in LIMIT statement in sql query as
     *            {@code LIMIT (from, to)}.
     * @param to
     *            value used in LIMIT statement in sql query as
     *            {@code LIMIT (from, to)}.
     * @param pinned
     *            if flag is true method returns pinned sections, otherwise non
     *            pinned.
     * @return list of sections from the database.
     */
    public Collection<BasicSection> get (User user, boolean pinned, long from, long to);
    
    public BasicSection get(User user, long sectionId);
    
    public void add(User user, String name,  boolean isPrivate, boolean isPinned) throws AccesException;
    
    
    public void remove(User user, long sectionId) throws AccesException;
    
    
    public void update(User user, long id, String name, boolean isPrivate, boolean isPinned, boolean isClosed) throws AccesException;
}
