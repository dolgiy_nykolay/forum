package com.dolgiy.model.section;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.List;

import com.dolgiy.beans.section.BasicSection;
import com.dolgiy.beans.user.User;
import com.dolgiy.beans.user.UserType;
import com.dolgiy.model.AccesException;

public class SecureSectionOperations extends ForumSectionOperationsProvider {

    public SecureSectionOperations (Connection connection) {
        super(connection);
    }

    @Override
    public Collection<BasicSection> get (User user, boolean pinned) {
        return super.getSections(pinned, user.getUserType() != UserType.BasicType.GUEST, 0, Long.MAX_VALUE);
    }

    @Override
    public Collection<BasicSection> get (User user, boolean pinned, long from, long to) {
        return super.getSections(pinned, user.getUserType() != UserType.BasicType.GUEST, from, to);
    }

    @Override
    public BasicSection get (User user, long sectionId) {
        return super.getSection(user, sectionId, user.getUserType() != UserType.BasicType.GUEST);
    }

    @Override
    public void add (User user, String name, boolean isPrivate, boolean isPinned) throws AccesException {
        try {
            PreparedStatement getModeratorsStatement = getConnection().prepareStatement(GET_SECTION_MODERATORS);
            getModeratorsStatement.setLong(1, -1);
            ResultSet getModeratorsResult = getModeratorsStatement.executeQuery();
            List<Long> moderators = getValues(getModeratorsResult, "id");
            if (!moderators.contains(user.getId())) {
                throw new AccesException(
                        "User: " + user.getId() + " doesn't have permission for execute this operation.");
            } else {
                super.add(user, name, isPrivate, isPinned);
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void remove (User user, long sectionId) throws AccesException {

        BasicSection thisSection = this.get(user, sectionId);
        if (thisSection == null || !thisSection.isModificationAllowed(user.getId())) {
            throw new AccesException("User: " + user.getId() + " doesn't have permission for execute this operation.");
        } else {
            super.remove(user, sectionId);
        }

    }

    @Override
    public void update (User user, long sectionId, String name, boolean isPrivate, boolean isPinned, boolean isOpened)
            throws AccesException {

        BasicSection thisSection = this.get(user, sectionId);
        if (thisSection == null || !thisSection.isModificationAllowed(user.getId())) {
            throw new AccesException("User: " + user.getId() + " doesn't have permission for execute this operation.");
        } else {
            super.update(user, sectionId, name, isPrivate, isPinned, isOpened);
        }

    }

}
