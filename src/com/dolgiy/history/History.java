package com.dolgiy.history;

/**
 * Represents history of some object.
 *
 * @param <T>
 *            Gets the type of object that history can operate with.
 */
public interface History<T> {

    /**
     * Puts new value in the history.
     * 
     * @param value
     *            Gets new value.
     */
    void add (T value);

    /**
     * Prevents the next call of {@link #add(T)} method. The next call of
     * {@link #add(T)} won't affect the history, so the next value won't get to the
     * history. It works for one call of{@link #add(T)} method.
     */
    void preventNextAdd ();

    /**
     * Returns last element in history.
     * 
     * @return Returns last element in history.
     */
    T last ();

    /**
     * Gets the last value in history that doesn't match with given value, or
     * returns null, if there is no such value.
     * 
     * @param value
     *            Gets the value for comparing.
     * @return Returns an element from history or null.
     */
    T last (T value);

    /**
     * Gets the last value in history that doesn't match with given value, or
     * returns defaultValue, if there is no such value.
     * 
     * @param value
     *            Gets the value for comparing.
     * @param defaultValue
     *            Gets the default value.
     * @return Returns an element from history or defaultValue.
     */
    T last (T value, T defaultValue);

    /**
     * Gets the element from history that contains on n step before current.
     * 
     * @param n
     *            Gets the steps count.
     * @return Returns element from history.
     */
    T getFromStepBack (int n);
}
