package com.dolgiy.history;

import java.util.Arrays;

/**
 * 
 * Represents history of URL.
 *
 */
public class UrlHistory implements History<String> {

    private String[] history;
    private String defaultValue;
    private int stepCount;
    private int currentPosition;
    private boolean isNextInsertPrevented;

    /**
     * Constructs object with given parameters.
     * 
     * @param stepCount
     *            Size of history.
     * @throws IllegalArgumentException
     *             If stepCount less then 1, because history must store at least 1
     *             step.
     */
    public UrlHistory (int stepCount, String defaultValue) {
        if (stepCount < 1) {
            throw new IllegalArgumentException("History must store at least 1 step. Current steps: " + stepCount);
        }
        this.defaultValue = defaultValue;
        this.stepCount = stepCount;
        this.currentPosition = 0;
        this.history = new String[stepCount];
        this.isNextInsertPrevented = false;
        Arrays.fill(this.history, "");
    }

    /**
     * Puts new URL in the history.
     * 
     * @param url
     *            Gets new URL.
     */
    public void add (String url) {
        if (!isNextInsertPrevented) {
            history[currentPosition++] = url;
            currentPosition %= stepCount;
        }
        isNextInsertPrevented = false;
    }

    /**
     * Prevents the next call of {@link #add(String)} method. The next call of
     * {@link #add(String)} won't affect the history, so the next URL won't get to
     * the history. It works for one call of{@link #add(String)} method.
     */
    public void preventNextAdd () {
        this.isNextInsertPrevented = true;
    }

    /**
     * Returns last element in history.
     * 
     * @return Returns last element in history.
     */
    public String last () {
        return getFromStepBack(1);
    }

    /**
     * Gets the last URL in history that doesn't match with given URL, or returns
     * defaultValue, if there is no such URL.
     * 
     * @param uri
     *            Gets the URL for comparing.
     * @return URL.
     */
    public String last (String uri) {
        for (int i = 1; i < stepCount; i++) {
            String step = getFromStepBack(i);
            if(step.equals("")) {
                return this.defaultValue;
            }
            if (!step.equals(uri)) {
                return getFromStepBack(i);
            }
        }
        return this.defaultValue;
    }

    /**
     * Gets the last URL in history that doesn't match with given URL, or returns
     * defaultValue, if there is no such URL.
     * 
     * @param uri
     *            Gets the URL for comparing.
     * @param defaultValue
     *            Gets the default value.
     * @return URL.
     */
    public String last (String uri, String defaultValue) {
        for (int i = 1; i < stepCount; i++) {
            String step = getFromStepBack(i);
            if(step.equals("")) {
                return defaultValue;
            }
            if (!step.equals(uri)) {
                return getFromStepBack(i);
            }
        }
        return defaultValue;
    }

    /**
     * Gets the element from history that contains on n step before current.
     * 
     * @param n
     *            Gets the steps count.
     * @return Returns element from history.
     */
    public String getFromStepBack (int n) {
        if (n > stepCount - 1) {
            throw new IllegalArgumentException("Can't get value earlier then history stores. History stores: "
                    + stepCount + "steps, requested step: " + n);
        }
        if (n > currentPosition) {
            return history[stepCount + ((currentPosition - n) % stepCount)];
        } else {
            return history[(currentPosition - n) % stepCount];
        }
    }

    @Override
    public String toString () {
        return "UrlHistory [history=" + Arrays.toString(history) + "]";
    }

}
