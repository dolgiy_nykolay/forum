package com.dolgiy.message;

public enum ForumMessages implements Message{
    SUCCESS{

        @Override
        public String getMessageKey () {
            return "forum.success";
        }

        @Override
        public MessageType getMessageType () {
            return BasicMessageTypes.INFO;
        }
        
    },
    UNKNOWN_ERROR{
        @Override
        public String getMessageKey () {
            return "forum.unknown-error";
        }

        @Override
        public MessageType getMessageType () {
            return BasicMessageTypes.ERROR;
        }
        
    }
    
    ;
}
