package com.dolgiy.message;

public enum InputErrorMessages implements Message{
    INPUT_FIELD_ERROR{
        @Override
        public String getMessageKey () {
            return "forum.name.long";
        }

        @Override
        public MessageType getMessageType () {
            return BasicMessageTypes.ERROR;
        }
        
    };

}
