package com.dolgiy.message;

public interface Message {
    
    String getMessageKey();
    
    MessageType getMessageType();
}
