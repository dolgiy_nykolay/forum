package com.dolgiy.message;

public enum BasicMessageTypes implements MessageType {
    ERROR, WARNING, INFO;
}
