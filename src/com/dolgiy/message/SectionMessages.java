package com.dolgiy.message;

public enum SectionMessages implements Message{
    EMPTY_NAME{

        @Override
        public String getMessageKey () {
            return "forum.name.empty";
        }

        @Override
        public MessageType getMessageType () {
            return BasicMessageTypes.ERROR;
        }
        
    },
    LONG_NAME{

        @Override
        public String getMessageKey () {
            return "forum.name.long";
        }

        @Override
        public MessageType getMessageType () {
            return BasicMessageTypes.ERROR;
        }
        
    };
}
