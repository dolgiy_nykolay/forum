package com.dolgiy.message;

import java.util.AbstractQueue;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.NoSuchElementException;
import java.util.Queue;


public class MessagesContainer extends AbstractQueue<Message> implements Queue<Message>, Iterable<Message>{

    private Queue<Message> messages;
    
    public MessagesContainer () {
        super();
        this.messages = new LinkedList<Message>();
    }

    @Override
    public Iterator<Message> iterator () {
        return new Iterator<Message>(){
            
            @Override
            public boolean hasNext () {
                return messages.size() > 0;
            }

            @Override
            public Message next () {
                Message m = messages.poll();
                if(m == null) {
                    throw new NoSuchElementException();
                }
                return m;
            }
            
        };
    }

    @Override
    public boolean offer (Message e) {
        return this.messages.offer(e);
    }

    @Override
    public Message poll () {
        return this.messages.poll();
    }

    @Override
    public Message peek () {
        return this.messages.peek();
    }

    @Override
    public int size () {
        return this.size();
    }

}
