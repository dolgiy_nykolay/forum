package com.dolgiy.message;

public enum UserMessages implements Message {

    REGISTRATION_COMPLETE {

        @Override
        public String getMessageKey () {
            return "user.signup.success-text";
        }

        @Override
        public MessageType getMessageType () {
            return BasicMessageTypes.INFO;
        }

    },

    LOGIN_PASSWORD_INCORRECT {

        @Override
        public String getMessageKey () {
            return "user.login.incorrect";
        }

        @Override
        public MessageType getMessageType () {
            return BasicMessageTypes.ERROR;
        }

    },

    EMAIL_IS_BUSY {

        @Override
        public String getMessageKey () {
            return "user.signup.email-in-use-text";
        }

        @Override
        public MessageType getMessageType () {
            return BasicMessageTypes.WARNING;
        }

    },

    REQUIRED_FIELD_IS_EMPTY {

        @Override
        public String getMessageKey () {
            return "user.signup.empty-fields-text";
        }

        @Override
        public MessageType getMessageType () {
            return BasicMessageTypes.ERROR;
        }

    },

    PASSWORDS_DONT_MATCH {

        @Override
        public String getMessageKey () {
            return "user.signup.passwords-dont-match-text";
        }

        @Override
        public MessageType getMessageType () {
            return BasicMessageTypes.ERROR;
        }

    };

}
