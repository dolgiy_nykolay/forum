package com.dolgiy.controller;

import java.io.IOException;
import java.io.InputStream;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import com.dolgiy.beans.image.Image;
import com.dolgiy.filters.ImageOperationsProviderFilter;
import com.dolgiy.message.ForumMessages;
import com.dolgiy.model.AccesException;

/**
 * Servlet implementation class ImageController
 */
@WebServlet("/ImageController")
public class ImageController extends BasicController {
    private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public ImageController () {
        super();
    }

    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
     *      response)
     */
    protected void doGet (HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String action = getURIPart(request.getRequestURI());
        long imgId = 1;
        try {
            imgId = new Long(action);
        }catch (NumberFormatException e) {
            // imgId is already 1.
        }
        try {
            Image image = getImageOperations().get(getUser(), imgId);
            byte[] imageBytes = new byte[image.getImage().available()];
            image.getImage().read(imageBytes);
            response.setContentType(image.getMimeType());
            response.setContentLength(imageBytes.length);
            response.getOutputStream().write(imageBytes);
        } catch (AccesException e) {
            response.getWriter().write("error");
        }
    }

    /**
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
     *      response)
     */
    protected void doPost (HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String action = getURIPart(request.getRequestURI());
        try {
            switch (action) {
            case "add":
                InputStream image = null;
                Part part = request.getPart("img");
                image = part.getInputStream();
                if(image == null) {
                    throw new NullPointerException();
                }
                getImageOperations().add(getUser(), image, this.getServletContext().getMimeType(part.getSubmittedFileName()));
            break;

            default:
            break;
            }
        } catch (NullPointerException e) {
            getMessagesQueue().add(ForumMessages.UNKNOWN_ERROR);
        } catch (AccesException e) {
            getMessagesQueue().add(ForumMessages.UNKNOWN_ERROR);
        } finally {
            response.sendRedirect(getUrlHistory().last());
        }
    }

}
