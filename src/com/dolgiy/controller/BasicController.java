package com.dolgiy.controller;

import java.io.IOException;
import java.util.Queue;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import com.dolgiy.beans.user.User;
import com.dolgiy.history.History;
import com.dolgiy.message.Message;
import com.dolgiy.model.image.ImageOperations;
import com.dolgiy.model.post.PostOperations;
import com.dolgiy.model.section.SectionOperations;
import com.dolgiy.model.topic.TopicOperations;
import com.dolgiy.model.user.UserOperations;

/**
 * 
 * Class that provides some operations for other servlets in application.
 *
 */
public abstract class BasicController extends HttpServlet {

    private static final long serialVersionUID = 1L;

    private User user;
    private History<String> urlHistory;
    private Queue<Message> messagesQueue;
    
    private DataSource dataSource;
    
    private UserOperations UserOperations;
    private SectionOperations sectionOperations;
    private TopicOperations topicOperations;
    private PostOperations postOperations;
    private ImageOperations imageOperations;



    @SuppressWarnings("unchecked")
    @Override
    protected void service (HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        
        this.setDataSource((DataSource) req.getSession().getAttribute("dataSourse"));
        this.setMessagesQueue((Queue<Message>) req.getSession().getAttribute("messages"));
        this.setPostOperations((PostOperations) req.getSession().getAttribute("postOperations"));
        this.setSectionOperations((SectionOperations) req.getSession().getAttribute("sectionOperations"));
        this.setTopicOperations((TopicOperations) req.getSession().getAttribute("topicOperations"));
        this.setUrlHistory((History<String>) req.getSession().getAttribute("history"));
        this.setUser((User) req.getAttribute("user"));
        this.setUserOperations((UserOperations) req.getSession().getAttribute("authenticator"));
        this.setImageOperations((ImageOperations)req.getSession().getAttribute("imageOperations"));
        
        super.service(req, resp);
    }




    /**
     * Pars the URL and get action name from it. Action it's 3th part of URL in
     * format:
     * 
     * <app-name> / <controller-name> / <action-name> [/ <parameters>]
     * 
     * @param uri
     *            request to server.
     * @return action name.
     */
    protected String getURIPart (String uri) {
        return getURIPart(uri, 3);
    }
    
    protected String getURIPart(String uri, int partNumber) {
        String[] uriParts = uri.trim().split("/");
        String part = "";
        if (uriParts.length > partNumber) {
            part = uriParts[partNumber];
        }
        return part;
    }
    
    protected User getUser () {
        return user;
    }




    protected void setUser (User user) {
        this.user = user;
    }




    protected History<String> getUrlHistory () {
        return urlHistory;
    }




    protected void setUrlHistory (History<String> urlHistory) {
        this.urlHistory = urlHistory;
    }




    protected Queue<Message> getMessagesQueue () {
        return messagesQueue;
    }




    protected void setMessagesQueue (Queue<Message> messagesQueue) {
        this.messagesQueue = messagesQueue;
    }




    protected DataSource getDataSource () {
        return dataSource;
    }




    protected void setDataSource (DataSource dataSource) {
        this.dataSource = dataSource;
    }




    protected UserOperations getUserOperations () {
        return UserOperations;
    }




    protected void setUserOperations (UserOperations userOperations) {
        UserOperations = userOperations;
    }




    protected SectionOperations getSectionOperations () {
        return sectionOperations;
    }




    protected void setSectionOperations (SectionOperations sectionOperations) {
        this.sectionOperations = sectionOperations;
    }




    protected TopicOperations getTopicOperations () {
        return topicOperations;
    }




    protected void setTopicOperations (TopicOperations topicOperations) {
        this.topicOperations = topicOperations;
    }




    protected PostOperations getPostOperations () {
        return postOperations;
    }




    protected void setPostOperations (PostOperations postOperations) {
        this.postOperations = postOperations;
    }

    
    protected ImageOperations getImageOperations () {
        return imageOperations;
    }


    protected void setImageOperations (ImageOperations imageOperations) {
        this.imageOperations = imageOperations;
    }
}
