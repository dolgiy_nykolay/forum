package com.dolgiy.controller;

import java.io.IOException;
import java.io.InputStream;
import java.util.Date;
import java.text.SimpleDateFormat;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import com.dolgiy.beans.user.UserType;
import com.dolgiy.beans.user.block.Block;
import com.dolgiy.beans.user.block.BlockedException;
import com.dolgiy.message.BasicMessageTypes;
import com.dolgiy.message.ForumMessages;
import com.dolgiy.message.InputErrorMessages;
import com.dolgiy.message.SectionMessages;
import com.dolgiy.message.UserMessages;
import com.dolgiy.model.AccesException;
import com.dolgiy.model.user.AuthenticationException;
import com.dolgiy.model.user.EmailExistsException;
import com.dolgiy.model.user.LoginExistsException;
import com.dolgiy.model.user.SessionException;
import com.dolgiy.model.user.UserOperations;
import com.sun.org.apache.xerces.internal.impl.xpath.regex.ParseException;

/**
 * Servlet implementation class UserController
 */
@MultipartConfig
@WebServlet("/UserController")
public class UserController extends BasicController {
    private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserController () {
        super();
    }

    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
     *      response)
     */
    protected void doGet (HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String action = getURIPart(request.getRequestURI());
        String forwardUrl = "";
        switch (action) {
        case "signup":
            if (getUser().getUserType() != UserType.BasicType.GUEST) {
                response.sendRedirect(getUrlHistory().last(request.getRequestURI()));
            } else {
                forwardUrl = "/views/userSignUp.jsp";
                request.getRequestDispatcher(forwardUrl).forward(request, response);
            }

        break;
        default:
            try {
                long userId = new Long(action);
                request.setAttribute("requestedUser", getUserOperations().get(userId));
                request.getRequestDispatcher("/views/userProfileView.jsp").forward(request, response);
            } catch (NumberFormatException e) {
                response.sendRedirect(getUrlHistory().last(request.getRequestURI()));
            }
        break;
        }
    }

    /**
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
     *      response)
     */
    protected void doPost (HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String action = getURIPart(request.getRequestURI());
        UserOperations authenticator = (UserOperations) request.getSession().getAttribute("authenticator");
        long userId;
        long sectionId;

        switch (action) {
        case "signup":
            if (getUser().getUserType() == UserType.BasicType.GUEST) {
                String name = request.getParameter("name");
                String surname = request.getParameter("surname");
                String nickname = request.getParameter("nickname");
                String password = request.getParameter("pass");
                String confirmPassword = request.getParameter("pass-confirm");
                String email = request.getParameter("email");

                request.getSession().setAttribute("name", name);
                request.getSession().setAttribute("surname", surname);
                request.getSession().setAttribute("nickname", nickname);
                request.getSession().setAttribute("email", email);

                if (name.equals("") || surname.equals("") || nickname.equals("") || password.equals("")
                        || confirmPassword.equals("") || email.equals("")) {
                    getMessagesQueue().add(UserMessages.REQUIRED_FIELD_IS_EMPTY);
                    response.sendRedirect("/forum/user/signup");
                    return;
                }
                if (password.equals(confirmPassword)) {
                    try {
                        authenticator.signUp(name, surname, nickname, email, email, password);

                        getMessagesQueue().add(UserMessages.REGISTRATION_COMPLETE);
                        response.sendRedirect(getUrlHistory().last(request.getRequestURI()));

                    } catch (LoginExistsException e) {

                        getMessagesQueue().add(UserMessages.EMAIL_IS_BUSY);
                        response.sendRedirect("/forum/user/signup");
                    } catch (EmailExistsException e) {

                        getMessagesQueue().add(UserMessages.EMAIL_IS_BUSY);
                        response.sendRedirect("/forum/user/signup");
                    }
                } else {

                    getMessagesQueue().add(UserMessages.PASSWORDS_DONT_MATCH);
                    response.sendRedirect("/forum/user/signup");
                }

            } else {
                response.sendRedirect(getUrlHistory().last(request.getRequestURI()));
            }
        break;
        case "signin":
            if (getUser().getUserType() == UserType.BasicType.GUEST) {
                String login = request.getParameter("login");
                String password = request.getParameter("password");
                String sessionId = request.getSession().getId();
                String ipAddress = request.getRemoteAddr();
                try {
                    authenticator.signIn(login, password, sessionId, ipAddress);
                    response.sendRedirect(getUrlHistory().last(request.getRequestURI()));
                } catch (AuthenticationException e) {
                    getMessagesQueue().add(UserMessages.LOGIN_PASSWORD_INCORRECT);
                    request.getSession().setAttribute("login", login);
                    response.sendRedirect(getUrlHistory().last(request.getRequestURI()));
                } catch (SessionException e) {
                    getMessagesQueue().add(ForumMessages.UNKNOWN_ERROR);
                    response.sendRedirect(getUrlHistory().last(request.getRequestURI()));
                } catch (BlockedException e) {
                    for (Block b : e.getBlocks()) {
                        getMessagesQueue().add(ForumMessages.UNKNOWN_ERROR);
                    }
                    response.sendRedirect(getUrlHistory().last(request.getRequestURI()));
                }
            } else {
                response.sendRedirect(getUrlHistory().last(request.getRequestURI()));
            }

        break;
        case "signout":
            if (getUser().getUserType() == UserType.BasicType.GUEST) {
                response.sendRedirect(getUrlHistory().last(request.getRequestURI()));
            } else {
                getUserOperations().signOut(getUser(), request.getSession().getId(), request.getRemoteAddr());
                request.getSession().invalidate();
                response.sendRedirect(getUrlHistory().last(request.getRequestURI()));
            }
        break;
        case "update":

            response.sendRedirect(getUrlHistory().last(request.getRequestURI()));
        break;
        case "setphoto":
            try {
                InputStream image = null;
                Part part = request.getPart("img");
                image = part.getInputStream();
                if (image == null) {
                    throw new NullPointerException();
                }

                long imgId = getImageOperations().add(getUser(), image,
                        this.getServletContext().getMimeType(part.getSubmittedFileName()));
                getUserOperations().setProfilePhoto(getUser(), getUser().getId(), imgId);
            } catch (AccesException e) {
                getMessagesQueue().add(ForumMessages.UNKNOWN_ERROR);
            } catch (NullPointerException e) {
                getMessagesQueue().add(ForumMessages.UNKNOWN_ERROR);
            }
            response.sendRedirect(getUrlHistory().last(request.getRequestURI()));
        break;
        case "setpassword":
            String oldPassword = request.getParameter("oldPassword");
            String newPassword = request.getParameter("newPassword");
            String confirmNewPassword = request.getParameter("confirmPassword");
            if (newPassword.equals(confirmNewPassword)) {
                try {
                    getUserOperations().setPassword(getUser(), getUser().getId(), oldPassword, newPassword);
                    getMessagesQueue().add(ForumMessages.SUCCESS);
                } catch (AccesException e) {
                    getMessagesQueue().add(ForumMessages.UNKNOWN_ERROR);
                }
            } else {
                getMessagesQueue().add(UserMessages.PASSWORDS_DONT_MATCH);
            }

            response.sendRedirect(getUrlHistory().last(request.getRequestURI()));
        break;
        case "block":
            try {
                userId = new Long(request.getParameter("userId"));
                String cause = request.getParameter("cause");
                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                String blockExpireDate = request.getParameter("date");
                getUserOperations().blockUser(getUser(), userId, cause,
                        new java.sql.Date(dateFormat.parse(blockExpireDate).getTime()));
            } catch (NumberFormatException e) {
                getMessagesQueue().add(ForumMessages.UNKNOWN_ERROR);
            } catch (AccesException e) {
                getMessagesQueue().add(ForumMessages.UNKNOWN_ERROR);
            } catch (java.text.ParseException e) {
                getMessagesQueue().add(ForumMessages.UNKNOWN_ERROR);
            }
            response.sendRedirect(getUrlHistory().last(request.getRequestURI()));
        break;
        case "setrole":
            try {
                userId = new Long(request.getParameter("userId"));
                String roleString = request.getParameter("role");
                UserType role = UserType.BasicType.valueOf(roleString);
                getUserOperations().setRole(getUser(), userId, role);
            } catch (NumberFormatException e) {
                getMessagesQueue().add(ForumMessages.UNKNOWN_ERROR);
            } catch (AccesException e) {
                getMessagesQueue().add(ForumMessages.UNKNOWN_ERROR);
            } catch (IllegalArgumentException e) {
                getMessagesQueue().add(ForumMessages.UNKNOWN_ERROR);
            }
            response.sendRedirect(getUrlHistory().last(request.getRequestURI()));
        break;
        case "addpermission":
            try {
                userId = new Long(request.getParameter("userId"));
                sectionId = new Long(request.getParameter("sectionId"));
                getUserOperations().addPermission(getUser(), userId, sectionId);
            } catch (NumberFormatException e) {
                getMessagesQueue().add(ForumMessages.UNKNOWN_ERROR);
            } catch (AccesException e) {
                getMessagesQueue().add(ForumMessages.UNKNOWN_ERROR);
            }
            response.sendRedirect(getUrlHistory().last(request.getRequestURI()));
        break;
        case "removepermission":
            try {
                userId = new Long(request.getParameter("userId"));
                sectionId = new Long(request.getParameter("sectionId"));
                getUserOperations().removePermission(getUser(), userId, sectionId);
            } catch (NumberFormatException e) {
                getMessagesQueue().add(ForumMessages.UNKNOWN_ERROR);
            } catch (AccesException e) {
                getMessagesQueue().add(ForumMessages.UNKNOWN_ERROR);
            }
            response.sendRedirect(getUrlHistory().last(request.getRequestURI()));
        break;
        default:
            response.sendRedirect(getUrlHistory().last(request.getRequestURI()));
        break;
        }
    }

}
