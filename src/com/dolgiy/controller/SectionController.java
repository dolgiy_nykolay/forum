package com.dolgiy.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.dolgiy.beans.section.BasicSection;
import com.dolgiy.message.ForumMessages;
import com.dolgiy.message.SectionMessages;

/**
 * Servlet implementation class SectionController
 */
@WebServlet("/SectionController")
public class SectionController extends BasicController {
    private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public SectionController () {
        super();
    }

    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
     *      response)
     */
    protected void doGet (HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String action = getURIPart(request.getRequestURI());
        Collection<BasicSection> pinnedSections;
        Collection<BasicSection> unpinnedSections;
        switch (action) {
        case "astext":
            pinnedSections = getSectionOperations().get(getUser(), true);
            unpinnedSections = getSectionOperations().get(getUser(), false);
            StringBuilder builder = new StringBuilder();
            List<BasicSection> sections = new ArrayList<>();
            sections.addAll(pinnedSections);
            sections.addAll(unpinnedSections);
            builder.append("[");
            for(BasicSection s : sections) {
                builder.append(s);
                builder.append(',');
            }
            builder.delete(builder.length() - 1, builder.length());
            builder.append("]");
            response.setContentType("text/json");
            response.getWriter().write(builder.toString());
        break;
        case "":
            try {
                pinnedSections = getSectionOperations().get(getUser(), true);
                unpinnedSections = getSectionOperations().get(getUser(), false);
                request.setAttribute("pinnedSections", pinnedSections);
                request.setAttribute("notPinnedSections", unpinnedSections);
            } catch (Exception e) {
                getMessagesQueue().add(ForumMessages.UNKNOWN_ERROR);
            }
            request.getRequestDispatcher("/views/home.jsp").forward(request, response);
        break;
        default:
            response.sendRedirect(getUrlHistory().last(request.getRequestURI()));
        break;
        }

    }

    /**
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
     *      response)
     */
    protected void doPost (HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String action = getURIPart(request.getRequestURI());

        String id = request.getParameter("id");
        String name = request.getParameter("name");
        boolean isPrivate = request.getParameter("isPrivate") != null
                ? request.getParameter("isPrivate").equals("on") || request.getParameter("isPrivate").equals("true")
                : false;
        boolean isPinned = request.getParameter("isPinned") != null
                ? request.getParameter("isPinned").equals("on") || request.getParameter("isPinned").equals("true")
                : false;
        boolean isClosed = request.getParameter("isOpened") != null
                ? request.getParameter("isOpened").equals("on") || request.getParameter("isOpened").equals("true")
                : false;

        switch (action) {
        case "add":
            if (!name.equals("")) {
                if (name.length() < 255) {
                    try {
                        getSectionOperations().add(getUser(), name, isPrivate, isPinned);
                    } catch (Exception e) {
                        getMessagesQueue().add(ForumMessages.UNKNOWN_ERROR);
                    }
                } else {
                    getMessagesQueue().add(SectionMessages.LONG_NAME);
                }
            } else {
                getMessagesQueue().add(SectionMessages.EMPTY_NAME);
            }

            response.sendRedirect(getUrlHistory().last(request.getRequestURI()));
        break;
        case "remove":
            try {
                long sectionId = new Long(id);
                getSectionOperations().remove(getUser(), sectionId);
            } catch (Exception e) {
                getMessagesQueue().add(ForumMessages.UNKNOWN_ERROR);
            }

            response.sendRedirect(getUrlHistory().last(request.getRequestURI()));
        break;
        case "update":
            if (!name.equals("")) {
                if (name.length() < 255) {
                    try {
                        getSectionOperations().update(getUser(), new Long(id), name, isPrivate, isPinned, isClosed);
                    } catch (Exception e) {
                        getMessagesQueue().add(ForumMessages.UNKNOWN_ERROR);
                    }
                } else {
                    getMessagesQueue().add(SectionMessages.LONG_NAME);
                }
            } else {
                getMessagesQueue().add(SectionMessages.EMPTY_NAME);
            }

            response.sendRedirect(getUrlHistory().last(request.getRequestURI()));
        break;
        default:
            response.sendRedirect(getUrlHistory().last(request.getRequestURI()));
        break;
        }
    }

}
