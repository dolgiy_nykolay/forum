package com.dolgiy.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.dolgiy.beans.topic.BasicTopic;
import com.dolgiy.message.ForumMessages;
import com.dolgiy.message.InputErrorMessages;
import com.dolgiy.model.AccesException;

/**
 * Servlet implementation class TopicController
 */
@WebServlet("/TopicController")
public class TopicController extends BasicController {
    private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public TopicController () {
        super();
    }

    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
     *      response)
     */
    protected void doGet (HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String action = getURIPart(request.getRequestURI());

        try {
            long sectionId = new Long(action);

            Iterable<BasicTopic> pinedTopics = getTopicOperations().get(getUser(), sectionId, true);
            Iterable<BasicTopic> notPinnedTopics = getTopicOperations().get(getUser(), sectionId, false);
            request.setAttribute("section", getSectionOperations().get(getUser(), sectionId));
            request.setAttribute("pinnedTopics", pinedTopics);
            request.setAttribute("notPinnedTopics", notPinnedTopics);

            request.getRequestDispatcher("/views/topicsList.jsp").forward(request, response);

        } catch (NumberFormatException e) {
            response.sendRedirect(getUrlHistory().last(request.getRequestURI()));
        }
    }

    /**
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
     *      response)
     */
    protected void doPost (HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String action = getURIPart(request.getRequestURI());

        String name = null;
        long sectionId = -1;
        long topicId = -1;
        boolean isPrivate = true;
        boolean isOpen = true;
        boolean isPinned = true;

        switch (action) {
        case "add":
            name = request.getParameter("name");
            try {
                sectionId = new Long(request.getParameter("sectionId"));
            } catch (NumberFormatException e) {
                getMessagesQueue().add(InputErrorMessages.INPUT_FIELD_ERROR);
                break;
            }
            isPrivate = request.getParameter("isPrivate") != null
                    ? request.getParameter("isPrivate").equals("on") || request.getParameter("isPrivate").equals("true")
                    : false;
            try {
                getTopicOperations().add(getUser(), name, sectionId, isPrivate, false);
            } catch (AccesException e) {
                getMessagesQueue().add(ForumMessages.UNKNOWN_ERROR);
            }
        break;
        case "update":
            name = request.getParameter("name");
            try {
                sectionId = new Long(request.getParameter("sectionId"));
                topicId = new Long(request.getParameter("id"));
            } catch (NumberFormatException e) {
                getMessagesQueue().add(InputErrorMessages.INPUT_FIELD_ERROR);
                break;
            }
            isPrivate = request.getParameter("isPrivate") != null
                    ? request.getParameter("isPrivate").equals("on") || request.getParameter("isPrivate").equals("true")
                    : false;
            isOpen = request.getParameter("isOpened") != null
                    ? request.getParameter("isOpened").equals("on") || request.getParameter("isOpened").equals("true")
                    : false;
            isPinned = request.getParameter("isPinned") != null
                    ? request.getParameter("isPinned").equals("on") || request.getParameter("isPinned").equals("true")
                    : false;
            try {
                getTopicOperations().update(getUser(), topicId, sectionId, name, isPrivate, isOpen, isPinned);
            } catch (AccesException e1) {
                getMessagesQueue().add(ForumMessages.UNKNOWN_ERROR);
            }
        break;
        case "remove":
            try {
                topicId = new Long(request.getParameter("id"));
            } catch (NumberFormatException e) {
                getMessagesQueue().add(InputErrorMessages.INPUT_FIELD_ERROR);
                break;
            }
            try {
                getTopicOperations().remove(getUser(), topicId);
            } catch (AccesException e) {
                getMessagesQueue().add(ForumMessages.UNKNOWN_ERROR);
            }
        break;
        default:
        // Nothing to do, user'll be redirected to the last valid URL anyway.
        break;
        }
        response.sendRedirect(getUrlHistory().last(request.getRequestURI()));
    }

}
