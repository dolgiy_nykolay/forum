package com.dolgiy.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.dolgiy.message.ForumMessages;
import com.dolgiy.model.AccesException;

/**
 * Servlet implementation class PostController
 */
@WebServlet("/PostController")
public class PostController extends BasicController {
    private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public PostController () {
        super();
    }

    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
     *      response)
     */
    protected void doGet (HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String action = getURIPart(request.getRequestURI());

        try {
            long topicId = new Long(action);
            String pageString = request.getParameter("page");
            long page = 1;
            int postsPerPage = 3;
            int pages;
            try {
                page = new Long(pageString);
                page = page < 1 ? 1 : page;
            } catch (NumberFormatException e) {
                // Nothing to do, variable page already assigned.
            }
            pages = (int) Math.ceil(getPostOperations().count(getUser(), topicId) / (double)postsPerPage);

            request.setAttribute("posts", getPostOperations().get(getUser(), topicId,
                    page * postsPerPage - postsPerPage, postsPerPage));
            request.setAttribute("parentTopic", getTopicOperations().get(getUser(), topicId));
            request.setAttribute("page", page);
            request.setAttribute("pages", pages);
            
            request.getRequestDispatcher("/views/postList.jsp").forward(request, response);
        } catch (NumberFormatException e) {
            response.sendRedirect(getUrlHistory().last(request.getRequestURI()));
        }

    }

    /**
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
     *      response)
     */
    protected void doPost (HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String action = getURIPart(request.getRequestURI());
        try {
            String content = request.getParameter("content");
            String topicIdString = request.getParameter("topicId");
            String relatedPostString = request.getParameter("relatedPost");
            String postIdString = request.getParameter("postId");
            long topicId;
            long relatedPost;
            long postId;

            switch (action) {
            case "add":
                topicId = new Long(topicIdString);
                relatedPost = new Long (relatedPostString);
                getPostOperations().add(getUser(), content, topicId, relatedPost);
            break;
            case "remove":
                postId = new Long(postIdString);
                getPostOperations().remove(getUser(), postId);
                break;
            default:
            // Nothing to do redirect always works.
            break;
            }
        } catch (AccesException e) {
            getMessagesQueue().add(ForumMessages.UNKNOWN_ERROR);
        }catch (NumberFormatException e) {
            getMessagesQueue().add(ForumMessages.UNKNOWN_ERROR);
        } finally {
            response.sendRedirect(getUrlHistory().last(request.getRequestURI()));
        }
    }

}
